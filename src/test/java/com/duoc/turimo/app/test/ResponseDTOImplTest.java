package com.duoc.turimo.app.test;



import com.duoc.turimo.app.bl.services.ResponseDTOImpl;
import com.duoc.turimo.app.bt.EmailService;
import com.duoc.turimo.app.bt.IResponseDTO;
import com.duoc.turimo.app.bt.IUsuarioRepository;
import com.duoc.turimo.app.dl.model.Reserva;
import com.duoc.turimo.app.dl.model.Usuario;
import com.duoc.turimo.app.dl.model.UsuarioDTO;
import com.duoc.turimo.app.emuns.EntitiesEnums;
import com.duoc.turimo.app.utils.DateUtil;
import com.duoc.turimo.app.utils.Validator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.server.ResponseStatusException;

import java.text.ParseException;
import java.util.Date;
import static com.duoc.turimo.app.utils.DateUtil.isCorrectOrderBetweenTwoDates;
import static com.duoc.turimo.app.utils.DateUtil.qtyOfBetweenDays;
import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)
@SpringBootTest
public class ResponseDTOImplTest {

    @TestConfiguration
    static class EmployeeServiceImplTestContextConfiguration {
        @Bean
        public IResponseDTO employeeService() {
            return new ResponseDTOImpl();
        }
    }

    @Qualifier("employeeService")
    @Autowired
    IResponseDTO responseDTOService;

    @MockBean
    IUsuarioRepository usuarioRepository;

    @Before
    public void setUp() {
        Usuario usuario = new Usuario(9L,"asd",true,new Date(),"administrador2",15892998L,1L);
        Mockito.when(usuarioRepository.findByNombreUsuario(usuario.getNombreUsuario())).thenReturn(usuario);
    }

    @Test
    public void actionFindByIdName() {
        String name = "administrador2";
        UsuarioDTO usuario = (UsuarioDTO) responseDTOService.actionFindByIdName(EntitiesEnums.USUARIO,name).getEntidad();
        assertThat(usuario.getNombreUsuario()).isEqualTo(name);
    }

    @Test
    public void calcularDiasEntreFechas() {
        int diff = qtyOfBetweenDays("2020-06-25", "2020-06-29");
        assertThat(diff).isEqualTo(4);
    }

    @Test
    public void deberiaDevolverNegativoConfechasInvertidas()  {
        assertThat(isCorrectOrderBetweenTwoDates("2020-06-29" ,"2020-06-25")).isFalse();
    }

    @Test
    public void noDeberiaPoderCrearUnaReservaConUnCheckinConFechaAnteriorAhoy(){
        try {
            Validator.checkInDateValidation("2020-07-10");
        }catch (ResponseStatusException error){
            assertThat(error.getReason()).isEqualTo("No puede designar la fecha de Checkin anterior a hoy.");
        }
    }








}