package com.duoc.turimo.app.dl.model;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.sql.Date;

@Getter
@Setter
public class PersonaDTO implements Serializable {
	
	private Long rutPersona;
    private String nombrePersona;
    private String apellidoPersona;
    private String correoPersona;
    private Date fechaNacimientoPersona;
    private String nacionalidaPersona;

    
    

}
