package com.duoc.turimo.app.dl.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PasajeroDTOAdd {

    private String nombrePasajero;
    private String rutPasajero;
    private String telefonoPasajero;
    private Long idUnidad;
    private Long idReserva;
}
