package com.duoc.turimo.app.dl.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UsuarioDTO implements Serializable {

    private Long idUsario;
    private String contrasena;
    private Boolean estado;
    private Date fechaCreacion;

    private String nombreUsuario;
    private Long rutPersona;
    private Long idRol;

}
