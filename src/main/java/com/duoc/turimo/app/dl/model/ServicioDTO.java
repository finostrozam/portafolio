package com.duoc.turimo.app.dl.model;

import lombok.Getter;
import lombok.Setter;
import java.io.Serializable;
import java.math.BigDecimal;
@Getter
@Setter
public class ServicioDTO implements Serializable {
    private Long idServicio;
    private Integer montoIva;
    private Integer montoServicio;
    private String nombreServicio;
    private Long idProveedor;
    private String tipo;
    private String descripcion;

}
