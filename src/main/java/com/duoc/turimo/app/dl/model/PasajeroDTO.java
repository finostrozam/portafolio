package com.duoc.turimo.app.dl.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

public class PasajeroDTO implements Serializable {
    private Long idPasajero;
    private String nombrePasajero;
    private String rutPasajero;
    private String telefonoPasajero;
    private Long idUnidad;

    @Id
    @Column(name = "id_pasajero")
    public Long getIdPasajero() {
        return idPasajero;
    }

    public void setIdPasajero(Long idPasajero) {
        this.idPasajero = idPasajero;
    }

    @Basic
    @Column(name = "nombre_pasajero")
    public String getNombrePasajero() {
        return nombrePasajero;
    }

    public void setNombrePasajero(String nombrePasajero) {
        this.nombrePasajero = nombrePasajero;
    }

    @Basic
    @Column(name = "rut_pasajero")
    public String getRutPasajero() {
        return rutPasajero;
    }

    public void setRutPasajero(String rutPasajero) {
        this.rutPasajero = rutPasajero;
    }

    @Basic
    @Column(name = "telefono_pasajero")
    public String getTelefonoPasajero() {
        return telefonoPasajero;
    }

    public void setTelefonoPasajero(String telefonoPasajero) {
        this.telefonoPasajero = telefonoPasajero;
    }

    @Basic
    @Column(name = "id_unidad")
    public Long getIdUnidad() {
        return idUnidad;
    }

    public void setIdUnidad(Long idUnidad) {
        this.idUnidad = idUnidad;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PasajeroDTO pasajero = (PasajeroDTO) o;
        return Objects.equals(idPasajero, pasajero.idPasajero) &&

                Objects.equals(nombrePasajero, pasajero.nombrePasajero) &&
                Objects.equals(rutPasajero, pasajero.rutPasajero) &&
                Objects.equals(telefonoPasajero, pasajero.telefonoPasajero) &&
                Objects.equals(idUnidad, pasajero.idUnidad);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idPasajero,  nombrePasajero, rutPasajero, telefonoPasajero, idUnidad);
    }
}
