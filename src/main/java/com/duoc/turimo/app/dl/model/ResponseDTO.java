package com.duoc.turimo.app.dl.model;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
@Getter
@Setter
public class ResponseDTO<T> implements Serializable {

	private T entidad;
	public Object getEntidad() {
		return entidad;
	}
	public void setEntidad(T entidad) {
		this.entidad = entidad;
	}

	private static final long serialVersionUID = 1L;
}
