package com.duoc.turimo.app.dl.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "detalles_reserva",schema = "public" , catalog = "turismo_real")
public class ServicioDetalle implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_detalle_reserva")
    private  Long idReservaDetalle;

    @Column(name = "id_reserva")
    private  Long idReserva;

    @Column(name = "id_servicio")
    private  Long idServicio;

    @Column(name = "qty")
    private  Integer qty;

    @Column(name = "total")
    private  Integer total;

    @Column(name = "precio_unitario")
    private  Integer precioUnitario;

}
