package com.duoc.turimo.app.dl.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "reservas", schema = "public", catalog = "turismo_real")
public class Reserva implements Serializable {
    private Long idReserva;
    private Integer cantPasajeros;
    private Integer cantServicios;
    private Date fechaIngreso;
    private Date fechaSalida;
    private Integer montoTotalReserva;
    private Long rutPersona;
    private Long idUnidad;
    private Integer abono;
    private Integer subTotalDias;
    private Integer subTotalServicios;
    private Boolean avisoCheckInEnviado;

    @Id
    @Column(name = "id_reserva")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getIdReserva() {
        return idReserva;
    }

    public void setIdReserva(Long idReserva) {
        this.idReserva = idReserva;
    }

    @Basic
    @Column(name = "cant_pasajeros", columnDefinition = "integer default 0")
    public Integer getCantPasajeros() {
        return cantPasajeros;
    }

    public void setCantPasajeros(Integer cantPasajeros) {
        this.cantPasajeros = cantPasajeros;
    }

    @Basic
    @Column(name = "cant_servicios", columnDefinition = "integer default 0")
    public Integer getCantServicios() {
        return cantServicios;
    }

    public void setCantServicios(Integer cantServicios) {
        this.cantServicios = cantServicios;
    }

    @Basic
    @Column(name = "fecha_ingreso")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    @Basic
    @Column(name = "fecha_salida")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    public Date getFechaSalida() {
        return fechaSalida;
    }

    public void setFechaSalida(Date fechaSalida) {
        this.fechaSalida = fechaSalida;
    }



    @Basic
    @Column(name = "monto_total_reserva", columnDefinition = "integer default 0")
    public Integer getMontoTotalReserva() {
        return montoTotalReserva;
    }

    public void setMontoTotalReserva(Integer montoTotalReserva) {
        this.montoTotalReserva = montoTotalReserva;
    }

    @Basic
    @Column(name = "rut_persona")
    public Long getRutPersona() {
        return rutPersona;
    }

    public void setRutPersona(Long rutPersona) {
        this.rutPersona = rutPersona;
    }

    @Basic
    @Column(name = "id_unidad")
    public Long getIdUnidad() {
        return idUnidad;
    }

    public void setIdUnidad(Long idUnidad) {
        this.idUnidad = idUnidad;
    }

    @Column(name = "abono" , columnDefinition = "integer default 0")
    public Integer getAbono() {
        return abono;
    }

    public void setAbono(Integer abono) {
        this.abono = abono;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Reserva reserva = (Reserva) o;
        return Objects.equals(idReserva, reserva.idReserva) &&
                Objects.equals(cantPasajeros, reserva.cantPasajeros) &&
                Objects.equals(cantServicios, reserva.cantServicios) &&
                Objects.equals(fechaIngreso, reserva.fechaIngreso) &&
                Objects.equals(fechaSalida, reserva.fechaSalida) &&

                Objects.equals(montoTotalReserva, reserva.montoTotalReserva) &&
                Objects.equals(rutPersona, reserva.rutPersona) &&
                Objects.equals(idUnidad, reserva.idUnidad);
    }

    @Column(name = "sub_total_dias_unidad", columnDefinition = "integer default 0")
    public Integer getSubTotalDias() {
        return subTotalDias;
    }

    public void setSubTotalDias(Integer subTotalDias) {
        this.subTotalDias = subTotalDias;
    }

    @Column(name = "sub_total_servicios", columnDefinition = "integer default 0")
    public Integer getSubTotalServicios() {
        return subTotalServicios;
    }

    public void setSubTotalServicios(Integer subTotalServicios) {
        this.subTotalServicios = subTotalServicios;
    }

    @Column(name = "aviso_check_in_enviado")
    public Boolean getAvisoCheckInEnviado() {
        return avisoCheckInEnviado;
    }

    public void setAvisoCheckInEnviado(Boolean avisoCheckInEnviado) {
        this.avisoCheckInEnviado = avisoCheckInEnviado;
    }

    @Override
    public int hashCode() {
        return Objects.hash(idReserva, cantPasajeros, cantServicios, fechaIngreso, fechaSalida,  montoTotalReserva, rutPersona, idUnidad);
    }
}
