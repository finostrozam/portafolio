package com.duoc.turimo.app.dl.model;

import lombok.Getter;
import lombok.Setter;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
public class ReservaDTOAdd implements Serializable {

    private String fechaIngreso;
    private String fechaSalida;
    private Long rutPersona;
    private Long idUnidad;
    private Integer abono;

}
