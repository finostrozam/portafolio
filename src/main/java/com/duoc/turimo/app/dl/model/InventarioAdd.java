package com.duoc.turimo.app.dl.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class InventarioAdd {
    private String articulo;
    private Long cantidad;
    private Long idUnidad;
}
