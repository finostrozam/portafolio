package com.duoc.turimo.app.dl.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Getter
@Setter
public class ProveedorAdd extends Proveedor implements Serializable {

    private String correoProveedor;
    private String nombreProveedor;
    private String rutProveedor;
    private String telefonoProveedor;


}
