package com.duoc.turimo.app.dl.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "roles", schema = "public", catalog = "turismo_real")
public class Rol {
    private Long idRol;

    private String tipoRol;

    @Id
    @Column(name = "id_rol")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getIdRol() {
        return idRol;
    }

    public void setIdRol(Long idRol) {
        this.idRol = idRol;
    }



    @Basic
    @Column(name = "tipo_rol", unique = true)
    public String getTipoRol() {
        return tipoRol;
    }

    public void setTipoRol(String tipoRol) {
        this.tipoRol = tipoRol;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rol rol = (Rol) o;
        return Objects.equals(idRol, rol.idRol) &&

                Objects.equals(tipoRol, rol.tipoRol);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idRol,  tipoRol);
    }
}
