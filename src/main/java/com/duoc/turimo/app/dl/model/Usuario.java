package com.duoc.turimo.app.dl.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "usuarios", schema = "public", catalog = "turismo_real")
public class Usuario {
    private Long idUsario;
    private String contrasena;
    private Boolean estado;
    private Date fechaCreacion;

    private String nombreUsuario;
    private Long rutPersona;
    private Long idRol;


    @Id
    @Column(name = "id_usario")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getIdUsario() {
        return idUsario;
    }

    public void setIdUsario(Long idUsario) {
        this.idUsario = idUsario;
    }

    @Basic
    @Column(name = "contrasena")
    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    @Basic
    @Column(name = "estado")
    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    @Basic(optional = false)
    @Column(name = "fecha_creacion")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }


    @Basic
    @Column(name = "nombre_usuario" , unique = true)
    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    @Basic
    @Column(name = "rut_persona")
    public Long getRutPersona() {
        return rutPersona;
    }

    public void setRutPersona(Long rutPersona) {
        this.rutPersona = rutPersona;
    }

    @Basic
    @Column(name = "id_rol")
    public Long getIdRol() {
        return idRol;
    }

    public void setIdRol(Long idRol) {
        this.idRol = idRol;
    }

}
