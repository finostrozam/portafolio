package com.duoc.turimo.app.dl.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "comunas", schema = "public", catalog = "turismo_real")
public class Comuna implements Serializable {
    private Long idComuna;
    private String nombreComuna;
    private Long idRegion;

    @Id
    @Column(name = "id_comuna")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getIdComuna() {
        return idComuna;
    }

    public void setIdComuna(Long idComuna) {
        this.idComuna = idComuna;
    }



    @Basic
    @Column(name = "nombre_comuna")
    public String getNombreComuna() {
        return nombreComuna;
    }

    public void setNombreComuna(String nombreComuna) {
        this.nombreComuna = nombreComuna;
    }

    @Basic
    @Column(name = "id_region")
    public Long getIdRegion() {
        return idRegion;
    }

    public void setIdRegion(Long idRegion) {
        this.idRegion = idRegion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Comuna comuna = (Comuna) o;
        return Objects.equals(idComuna, comuna.idComuna) &&
               Objects.equals(nombreComuna, comuna.nombreComuna) &&
               Objects.equals(idRegion, comuna.idRegion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idComuna, nombreComuna, idRegion);
    }
}
