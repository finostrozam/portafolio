package com.duoc.turimo.app.dl.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "proveedores", schema = "public", catalog = "turismo_real")
public class Proveedor implements Serializable {
    private Long   idProveedor;
    private String correoProveedor;
    private String nombreProveedor;
    private String rutProveedor;
    private String telefonoProveedor;

    @Id
    @Column(name = "id_proveedor")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(Long idProveedor) {
        this.idProveedor = idProveedor;
    }

    @Basic
    @Column(name = "correo_proveedor")
    public String getCorreoProveedor() {
        return correoProveedor;
    }

    public void setCorreoProveedor(String correoProveedor) {
        this.correoProveedor = correoProveedor;
    }


    @Basic
    @Column(name = "nombre_proveedor")
    public String getNombreProveedor() {
        return nombreProveedor;
    }

    public void setNombreProveedor(String nombreProveedor) {
        this.nombreProveedor = nombreProveedor;
    }

    @Basic
    @Column(name = "rut_proveedor")
    public String getRutProveedor() {
        return rutProveedor;
    }

    public void setRutProveedor(String rutProveedor) {
        this.rutProveedor = rutProveedor;
    }

    @Basic
    @Column(name = "telefono_proveedor")
    public String getTelefonoProveedor() {
        return telefonoProveedor;
    }

    public void setTelefonoProveedor(String telefonoProveedor) {
        this.telefonoProveedor = telefonoProveedor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Proveedor proveedor = (Proveedor) o;
        return Objects.equals(idProveedor, proveedor.idProveedor) &&
                Objects.equals(correoProveedor, proveedor.correoProveedor) &&

                Objects.equals(nombreProveedor, proveedor.nombreProveedor) &&
                Objects.equals(rutProveedor, proveedor.rutProveedor) &&
                Objects.equals(telefonoProveedor, proveedor.telefonoProveedor);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idProveedor, correoProveedor,  nombreProveedor, rutProveedor, telefonoProveedor);
    }
}
