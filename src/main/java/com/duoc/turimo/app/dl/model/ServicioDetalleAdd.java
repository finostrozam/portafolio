package com.duoc.turimo.app.dl.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
public class ServicioDetalleAdd implements Serializable {


    private  Long idReserva;
    private  Long idServicio;
    private  Integer qty;

}
