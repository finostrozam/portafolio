package com.duoc.turimo.app.dl.model;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class UnidadDTOAdd implements Serializable {

    private Integer cantBano;
    private Integer cantCamaPlaza;
    private Integer cantCamaPlazaDoble;
    private Integer cantCamaPlazaMedia;
    private Integer cantPieza;
    private Integer codigoPostal;
    private String direccion;
    private Boolean disponible;
    private String descripcion;
    private Integer numeroUnidad;
    private Long idComuna;
    private Integer precio;

}
