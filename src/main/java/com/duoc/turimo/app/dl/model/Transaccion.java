package com.duoc.turimo.app.dl.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.Objects;

@Getter
@Setter
@Entity
@Table(name = "transacciones", schema = "public", catalog = "turismo_real")
public class Transaccion {
    @Id
    @Column(name = "idtransaccion")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idtransaccion;

    @Column(name = "fecha_transaccion")
    private Date fechaTransaccion;
    @Column(name = "estado_transaccion")
    private String estadoTransaccion;
    @Column(name = "mensaje_transaccion")
    private String mensajeTransaccion;
    @Column(name = "tipo_transaccion")
    private String tipoTransaccion;


}
