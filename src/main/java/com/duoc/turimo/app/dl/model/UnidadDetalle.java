package com.duoc.turimo.app.dl.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "unidad_detalle")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UnidadDetalle implements Serializable {

    @Id
    @Column(name = "id_detalle_imagen")
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    Long idDetalleImagen;

    @Column(name = "url_imagen")
    String urlImagen;

    @Column(name = "id_unidad_fk")
    Long idUnidad;

    @Column(name = "descripcion")
    String descripcion;

}
