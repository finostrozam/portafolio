package com.duoc.turimo.app.dl.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Transient;
import java.io.Serializable;
@Getter
@Setter
public class UnidadDTO implements Serializable {

    private Long idUnidad;
    private Integer cantBano;
    private Integer cantCamaPlaza;
    private Integer cantCamaPlazaDoble;
    private Integer cantCamaPlazaMedia;
    private Integer cantPieza;
    private Integer codigoPostal;
    private String direccion;
    private Boolean disponible;
    private String descripcion;
    private Integer numeroUnidad;
    private Long idComuna;
    private Integer precio;

}
