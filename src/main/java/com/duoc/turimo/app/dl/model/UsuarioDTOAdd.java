package com.duoc.turimo.app.dl.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
public class UsuarioDTOAdd  implements Serializable {
    private String contrasena;
    private String nombreUsuario;
    private Long rutPersona;
    private Long idRol;
    private Boolean estado;
    @JsonIgnore
    private Date fechaCreacion;

}
