package com.duoc.turimo.app.dl.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "unidades",catalog = "turismo_real",schema = "public")
public class Unidad implements Serializable {

    private Long idUnidad;
    private Integer cantBano;
    private Integer cantCamaPlaza;
    private Integer cantCamaPlazaDoble;
    private Integer cantCamaPlazaMedia;
    private Integer cantPieza;
    private Integer codigoPostal;
    private String direccion;
    private String descripcio;
    private Integer numeroUnidad;
    private String urlImagen;
    private Boolean disponible;
    private Long idComuna;
    private Integer precio;

    @Column(name = "precio")
    public Integer getPrecio() {
        return precio;
    }

    public void setPrecio(Integer precio) {
        this.precio = precio;
    }

    @Column(name = "descripcion")
    public String getDescripcio() {
        return descripcio;
    }

    public void setDescripcio(String descripcio) {
        this.descripcio = descripcio;
    }

    @Column(name = "id_comuna")
    public Long getIdComuna() {
        return idComuna;
    }

    public void setIdComuna(Long idComuna) {
        this.idComuna = idComuna;
    }

    @Id
    @Column(name = "id_unidad", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getIdUnidad() {
        return idUnidad;
    }

    public void setIdUnidad(Long idUnidad) {
        this.idUnidad = idUnidad;
    }

    @Basic
    @Column(name = "cant_bano", nullable = true)
    public Integer getCantBano() {
        return cantBano;
    }

    public void setCantBano(Integer cantBano) {
        this.cantBano = cantBano;
    }

    @Basic
    @Column(name = "cant_cama_plaza", nullable = true)
    public Integer getCantCamaPlaza() {
        return cantCamaPlaza;
    }

    public void setCantCamaPlaza(Integer cantCamaPlaza) {
        this.cantCamaPlaza = cantCamaPlaza;
    }

    @Basic
    @Column(name = "cant_cama_plaza_doble", nullable = true)
    public Integer getCantCamaPlazaDoble() {
        return cantCamaPlazaDoble;
    }

    public void setCantCamaPlazaDoble(Integer cantCamaPlazaDoble) {
        this.cantCamaPlazaDoble = cantCamaPlazaDoble;
    }

    @Basic
    @Column(name = "cant_cama_plaza_media", nullable = true)
    public Integer getCantCamaPlazaMedia() {
        return cantCamaPlazaMedia;
    }

    public void setCantCamaPlazaMedia(Integer cantCamaPlazaMedia) {
        this.cantCamaPlazaMedia = cantCamaPlazaMedia;
    }

    @Basic
    @Column(name = "cant_pieza", nullable = true)
    public Integer getCantPieza() {
        return cantPieza;
    }

    public void setCantPieza(Integer cantPieza) {
        this.cantPieza = cantPieza;
    }

    @Basic
    @Column(name = "codigo_postal", nullable = true)
    public Integer getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(Integer codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    @Basic
    @Column(name = "direccion", nullable = true, length = 200)
    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Basic
    @Column(name = "numero_unidad", nullable = true)
    public Integer getNumeroUnidad() {
        return numeroUnidad;
    }

    public void setNumeroUnidad(Integer numeroUnidad) {
        this.numeroUnidad = numeroUnidad;
    }

    @Basic
    @Column(name = "url_imagen", nullable = true, length = 20000)
    public String getUrlImagen() {
        return urlImagen;
    }

    public void setUrlImagen(String urlImagen) {
        this.urlImagen = urlImagen;
    }

    @Basic
    @Column(name = "disponible", nullable = true)
    public Boolean getDisponible() {
        return disponible;
    }

    public void setDisponible(Boolean disponible) {
        this.disponible = disponible;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Unidad unidad = (Unidad) o;
        return Objects.equals(idUnidad, unidad.idUnidad) &&
                Objects.equals(cantBano, unidad.cantBano) &&
                Objects.equals(cantCamaPlaza, unidad.cantCamaPlaza) &&
                Objects.equals(cantCamaPlazaDoble, unidad.cantCamaPlazaDoble) &&
                Objects.equals(cantCamaPlazaMedia, unidad.cantCamaPlazaMedia) &&
                Objects.equals(cantPieza, unidad.cantPieza) &&
                Objects.equals(codigoPostal, unidad.codigoPostal) &&
                Objects.equals(direccion, unidad.direccion) &&
                Objects.equals(numeroUnidad, unidad.numeroUnidad) &&
                Objects.equals(urlImagen, unidad.urlImagen) &&
                Objects.equals(disponible, unidad.disponible);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idUnidad, cantBano, cantCamaPlaza, cantCamaPlazaDoble, cantCamaPlazaMedia, cantPieza, codigoPostal, direccion,  numeroUnidad, urlImagen, disponible);
    }
}
