package com.duoc.turimo.app.dl.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UnidadDetalleAdd implements Serializable {

    Long idUnidad;
    String urlImagen;
    String descripcion;
}
