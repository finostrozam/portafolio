package com.duoc.turimo.app.dl.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "regiones", schema = "public", catalog = "turismo_real")
public class Region implements Serializable {
    private Long idRegion;

    private String nombreRegion;
    private Long idPais;

    @Id
    @Column(name = "id_region")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getIdRegion() {
        return idRegion;
    }

    public void setIdRegion(Long idRegion) {
        this.idRegion = idRegion;
    }



    @Basic
    @Column(name = "nombre_region")
    public String getNombreRegion() {
        return nombreRegion;
    }

    public void setNombreRegion(String nombreRegion) {
        this.nombreRegion = nombreRegion;
    }

    @Basic
    @Column(name = "id_pais")
    public Long getIdPais() {
        return idPais;
    }

    public void setIdPais(Long idPais) {
        this.idPais = idPais;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Region region = (Region) o;
        return Objects.equals(idRegion, region.idRegion) &&

                Objects.equals(nombreRegion, region.nombreRegion) &&
                Objects.equals(idPais, region.idPais);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idRegion, nombreRegion, idPais);
    }
}
