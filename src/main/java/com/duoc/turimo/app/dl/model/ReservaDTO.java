package com.duoc.turimo.app.dl.model;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
public class ReservaDTO implements Serializable {
    private Integer cantPasajeros;
    private Date fechaIngreso;
    private Date fechaSalida;
    
    private BigDecimal montoTotalReserva;
    private Long rutPersona;
    private Long idUnidad;

}
