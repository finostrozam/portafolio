package com.duoc.turimo.app.dl.model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
@Table(name = "servicios", schema = "public", catalog = "turismo_real")
public class Servicio {
    private Long idServicio;

    private Integer montoIva;
    private Integer montoServicio;
    private String nombreServicio;
    private Long idProveedor;
    private String tipo;
    private String descripcion;

    @Id
    @Column(name = "id_servicio")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getIdServicio() {
        return idServicio;
    }

    public void setIdServicio(Long idServicio) {
        this.idServicio = idServicio;
    }



    @Basic
    @Column(name = "monto_iva")
    public Integer getMontoIva() {
        return montoIva;
    }

    public void setMontoIva(Integer montoIva) {
        this.montoIva = montoIva;
    }

    @Basic
    @Column(name = "monto_servicio")
    public Integer getMontoServicio() {
        return montoServicio;
    }

    public void setMontoServicio(Integer montoServicio) {
        this.montoServicio = montoServicio;
    }

    @Basic
    @Column(name = "nombre_servicio")
    public String getNombreServicio() {
        return nombreServicio;
    }

    public void setNombreServicio(String nombreServicio) {
        this.nombreServicio = nombreServicio;
    }

    @Basic
    @Column(name = "id_proveedor")
    public Long getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(Long idProveedor) {
        this.idProveedor = idProveedor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Servicio servicio = (Servicio) o;
        return Objects.equals(idServicio, servicio.idServicio) &&

                Objects.equals(montoIva, servicio.montoIva) &&
                Objects.equals(montoServicio, servicio.montoServicio) &&
                Objects.equals(nombreServicio, servicio.nombreServicio) &&
                Objects.equals(idProveedor, servicio.idProveedor);
    }

    @Column(name = "tipo")
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Column(name = "descripcion")
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public int hashCode() {
        return Objects.hash(idServicio,  montoIva, montoServicio, nombreServicio, idProveedor);
    }
}
