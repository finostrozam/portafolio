package com.duoc.turimo.app.dl.model;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name = "personas", schema = "public", catalog = "turismo_real")
public class Persona {
    private Long rutPersona;
    private String apellidoPersona;
    private String correoPersona;
    private Date fechaNacimientoPersona;

    private String nacionalidaPersona;
    private String nombrePersona;

    @Id
    @Column(name = "rut_persona")
    public Long getRutPersona() {
        return rutPersona;
    }

    public void setRutPersona(Long rutPersona) {
        this.rutPersona = rutPersona;
    }

    @Basic
    @Column(name = "apellido_persona")
    public String getApellidoPersona() {
        return apellidoPersona;
    }

    public void setApellidoPersona(String apellidoPersona) {
        this.apellidoPersona = apellidoPersona;
    }

    @Basic
    @Column(name = "correo_persona")
    public String getCorreoPersona() {
        return correoPersona;
    }

    public void setCorreoPersona(String correoPersona) {
        this.correoPersona = correoPersona;
    }

    @Basic
    @Column(name = "fecha_nacimiento_persona")
    public Date getFechaNacimientoPersona() {
        return fechaNacimientoPersona;
    }

    public void setFechaNacimientoPersona(Date fechaNacimientoPersona) {
        this.fechaNacimientoPersona = fechaNacimientoPersona;
    }



    @Basic
    @Column(name = "nacionalida_persona")
    public String getNacionalidaPersona() {
        return nacionalidaPersona;
    }

    public void setNacionalidaPersona(String nacionalidaPersona) {
        this.nacionalidaPersona = nacionalidaPersona;
    }

    @Basic
    @Column(name = "nombre_persona")
    public String getNombrePersona() {
        return nombrePersona;
    }

    public void setNombrePersona(String nombrePersona) {
        this.nombrePersona = nombrePersona;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Persona persona = (Persona) o;
        return Objects.equals(rutPersona, persona.rutPersona) &&
                Objects.equals(apellidoPersona, persona.apellidoPersona) &&
                Objects.equals(correoPersona, persona.correoPersona) &&
                Objects.equals(fechaNacimientoPersona, persona.fechaNacimientoPersona) &&

                Objects.equals(nacionalidaPersona, persona.nacionalidaPersona) &&
                Objects.equals(nombrePersona, persona.nombrePersona);
    }

    @Override
    public int hashCode() {
        return Objects.hash(rutPersona, apellidoPersona, correoPersona, fechaNacimientoPersona,  nacionalidaPersona, nombrePersona);
    }
}
