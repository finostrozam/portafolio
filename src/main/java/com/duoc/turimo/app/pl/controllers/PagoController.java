package com.duoc.turimo.app.pl.controllers;

import com.duoc.turimo.app.bt.IPagosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class PagoController {

    @Autowired
    IPagosService  pagosService;

    @GetMapping(value = "/pagos/{idReserva}/{monto}")
    public void abonarAReserva(@PathVariable Long idReserva, @PathVariable Integer monto){
        pagosService.pagarReserva(idReserva,monto);
    }
}
