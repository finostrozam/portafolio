package com.duoc.turimo.app.pl.controllers;

import com.duoc.turimo.app.bt.IResponseDTO;
import com.duoc.turimo.app.dl.model.ResponseDTO;
import com.duoc.turimo.app.emuns.EntitiesEnums;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

@RestController
public class PaisController {


	@Qualifier("responseDTOImpl")
	@Autowired
	IResponseDTO responseDTOService;

	@PostMapping(value = "/paises")
	public ResponseDTO getAllPaises() {
	 return	responseDTOService.actionGetAllToResponse(EntitiesEnums.PAIS);
	}

	@GetMapping(value = "/paises/{id}")
	public ResponseDTO getPaisById(@PathVariable Long id){
		return responseDTOService.actionFindByIdResponse(EntitiesEnums.PAIS,id);
	}









}
