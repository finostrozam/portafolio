package com.duoc.turimo.app.pl.controllers;

import com.duoc.turimo.app.bt.IPasajeroService;
import com.duoc.turimo.app.bt.IReservaRepository;
import com.duoc.turimo.app.bt.IResponseDTO;
import com.duoc.turimo.app.dl.model.PasajeroDTOAdd;
import com.duoc.turimo.app.dl.model.ResponseDTO;
import com.duoc.turimo.app.emuns.EntitiesEnums;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

@RestController
public class PasajeroController {

    @Qualifier("responseDTOImpl")
    @Autowired
    IResponseDTO responseDTO;

    @PostMapping(value = "/pasajeros")
    public void addPasajero(@RequestBody PasajeroDTOAdd pasajeroDTOAdd){
        responseDTO.actionSaveToResponse(pasajeroDTOAdd);
    }

    @GetMapping(value = "/pasajeros/{idReserva}")
    public ResponseDTO getAllPersonasByIdUnidadAndReserva(@PathVariable Long idReserva){
       return responseDTO.actionGetAllToResponseByIdGenerico(EntitiesEnums.PASAJERO, idReserva);
    }
}
