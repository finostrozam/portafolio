package com.duoc.turimo.app.pl.controllers;

import com.duoc.turimo.app.bt.IResponseDTO;
import com.duoc.turimo.app.dl.model.InventarioAdd;
import com.duoc.turimo.app.dl.model.ResponseDTO;
import com.duoc.turimo.app.emuns.EntitiesEnums;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

@RestController
public class InventarioController {

    @Qualifier("responseDTOImpl")
    @Autowired
    IResponseDTO responseDTO;

    @GetMapping(value = "/inventarios")
    public ResponseDTO getAllInventarios(){
        return responseDTO.actionGetAllToResponse(EntitiesEnums.INVENTARIO);
    }

    @GetMapping(value = "/inventario/{idUnidad}")
    public ResponseDTO getInventarioById(@PathVariable Long idUnidad){
        return responseDTO.actionFindByIdResponse(EntitiesEnums.INVENTARIO,idUnidad);
    }

    @GetMapping(value = "/inventarios/{idUnidad}")
    public ResponseDTO getInventariosById(@PathVariable Long idUnidad){
        return responseDTO.actionGetAllToResponseByIdGenerico(EntitiesEnums.INVENTARIO,idUnidad);
    }

    @PostMapping(value = "/inventarios")
    public void addInventario(@RequestBody InventarioAdd inventarioAdd){
        responseDTO.actionSaveToResponse(inventarioAdd);
    }

}
