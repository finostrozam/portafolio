package com.duoc.turimo.app.pl.controllers;

import com.duoc.turimo.app.bt.IDetalleUnidadService;
import com.duoc.turimo.app.bt.IResponseDTO;
import com.duoc.turimo.app.bt.IUnidadService;
import com.duoc.turimo.app.dl.model.ResponseDTO;
import com.duoc.turimo.app.dl.model.UnidadDTO;
import com.duoc.turimo.app.dl.model.UnidadDTOAdd;
import com.duoc.turimo.app.dl.model.UnidadDetalleAdd;
import com.duoc.turimo.app.emuns.EntitiesEnums;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UnidadController {

    @Qualifier("responseDTOImpl")
    @Autowired
    IResponseDTO responseService;

    @Autowired
    IDetalleUnidadService detalleUnidadService;

    @PostMapping(value = "/unidades")
    public void addUnidad(@RequestBody UnidadDTOAdd unidadDTO){
        responseService.actionSaveToResponse(unidadDTO);
    }

    @GetMapping(value = "/unidades/{id}")
    public ResponseDTO getUnidad(@PathVariable Long id){
        return responseService.actionFindByIdResponse(EntitiesEnums.UNIDAD,id);
    }

    @GetMapping(value = "/unidades")
    public ResponseDTO getAllUnidades(){
        return responseService.actionGetAllToResponse(EntitiesEnums.UNIDAD);
    }

    @PutMapping(value = "/unidades")
    public void updateUnidad(@RequestBody UnidadDTO unidad){
        responseService.actionUpdateToResponse(unidad);
    }

    @PostMapping(value = "/unidades/image")
    public void addAnImageToUnidad(@RequestBody UnidadDetalleAdd unidadDetalleAdd){
        responseService.actionSaveToResponse(unidadDetalleAdd);
    }

    @PostMapping(value = "/unidades/images")
    public void addImagesToUnidad(@RequestBody List<UnidadDetalleAdd> unidadesDetalleAdd){
        detalleUnidadService.saveAllImages(unidadesDetalleAdd);
    }

    @GetMapping(value = "/unidades/images/{idUnidad}")
    public ResponseDTO getAllImagesByIdUnidades(@PathVariable Long idUnidad){
        return responseService.actionGetAllToResponseByIdGenerico(EntitiesEnums.UNIDAD_DETALLE,idUnidad);
    }
}
