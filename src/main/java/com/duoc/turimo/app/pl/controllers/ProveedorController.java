package com.duoc.turimo.app.pl.controllers;

import com.duoc.turimo.app.bt.IProveedorService;
import com.duoc.turimo.app.bt.IResponseDTO;
import com.duoc.turimo.app.dl.model.Proveedor;
import com.duoc.turimo.app.dl.model.ProveedorAdd;
import com.duoc.turimo.app.dl.model.ResponseDTO;
import com.duoc.turimo.app.emuns.EntitiesEnums;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

@RestController
public class ProveedorController {

    @Qualifier("responseDTOImpl")
    @Autowired
    IResponseDTO iProveedorService;

    @GetMapping(value = "/proveedores/{id}")
    public ResponseDTO getProveedorById(@PathVariable Long id){
        return iProveedorService.actionFindByIdResponse(EntitiesEnums.PROVEEDOR,id);
    }

    @GetMapping(value = "/proveedores")
    public ResponseDTO getAllProveedores(){
        return iProveedorService.actionGetAllToResponse(EntitiesEnums.PROVEEDOR);
    }
    @PostMapping(value = "/proveedores")
    public void addProveedores(@RequestBody ProveedorAdd proveedorAdd){
        iProveedorService.actionSaveToResponse(proveedorAdd);
    }

    @PutMapping(value = "/proveedores")
    public void updateProveedor(@RequestBody Proveedor proveedor ){
        iProveedorService.actionUpdateToResponse(proveedor);
    }


}
