package com.duoc.turimo.app.pl.controllers;

import com.duoc.turimo.app.bt.IResponseDTO;
import com.duoc.turimo.app.dl.model.ResponseDTO;
import com.duoc.turimo.app.emuns.EntitiesEnums;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RegionController {

	@Qualifier("responseDTOImpl")
	@Autowired
	IResponseDTO responseDTOService;

	@PostMapping(value = "/regiones")
	public ResponseDTO getAllRegiones() {
	 return	responseDTOService.actionGetAllToResponse(EntitiesEnums.REGION);
	}

	@GetMapping(value = "/regiones/{id}")
	public ResponseDTO getRegionById(@PathVariable Long id){
		return responseDTOService.actionFindByIdResponse(EntitiesEnums.REGION,id);
	}









}
