package com.duoc.turimo.app.pl.controllers;

import com.duoc.turimo.app.bl.services.ResponseDTOImpl;
import com.duoc.turimo.app.bt.IResponseDTO;
import com.duoc.turimo.app.dl.model.*;
import com.duoc.turimo.app.emuns.EntitiesEnums;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

@RestController
public class ServicioController {

    @Qualifier("responseDTOImpl")
    @Autowired
    IResponseDTO responseDTO;

    @PostMapping(value = "/servicios")
    public void addServiceToReserva(@RequestBody ServicioDetalleAdd servicioDetalle){
        responseDTO.actionSaveToResponse(servicioDetalle);
    }

    @GetMapping(value = "/servicios/{id}")
    public ResponseDTO getServicioById(@PathVariable Long id){
        return responseDTO.actionFindByIdResponse(EntitiesEnums.SERVICIO,id);
    }

    @GetMapping(value = "/servicios/reservas/{id}")
    public ResponseDTO getServciosByIdReserva(@PathVariable Long id){
        return responseDTO.actionFindByIdResponse(EntitiesEnums.SERVICIO_DETALLE,id);
    }

    @GetMapping(value = "/servicios/detalles")
    public ResponseDTO getAllServiciosDetalle(){
        return responseDTO.actionGetAllToResponse(EntitiesEnums.SERVICIO_DETALLE);
    }

    @GetMapping(value = "/servicios")
    public ResponseDTO getAllServicios(){
        return responseDTO.actionGetAllToResponse(EntitiesEnums.SERVICIO);
    }

    @GetMapping(value = "/servicios/detalle/{idReserva}")
    public ResponseDTO getAllServiciosByIdReserva(@PathVariable Long idReserva){
        return responseDTO.actionGetAllToResponseByIdGenerico(EntitiesEnums.SERVICIO_DETALLE,idReserva);
    }


}
