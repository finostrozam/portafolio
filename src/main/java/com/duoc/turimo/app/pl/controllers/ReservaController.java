package com.duoc.turimo.app.pl.controllers;

import com.duoc.turimo.app.bt.IReservaRepository;
import com.duoc.turimo.app.bt.IReservaService;
import com.duoc.turimo.app.bt.IResponseDTO;
import com.duoc.turimo.app.dl.model.Reserva;
import com.duoc.turimo.app.dl.model.ReservaDTO;
import com.duoc.turimo.app.dl.model.ReservaDTOAdd;
import com.duoc.turimo.app.dl.model.ResponseDTO;
import com.duoc.turimo.app.emuns.EntitiesEnums;
import com.duoc.turimo.app.utils.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;

@RestController
public class ReservaController {

    @Qualifier("responseDTOImpl")
    @Autowired
    IResponseDTO responseDTO;

    @Autowired
    IReservaRepository reservaRepository;

    @GetMapping(value = "/reservas")
    public ResponseDTO getAllReservas(){
        return responseDTO.actionGetAllToResponse(EntitiesEnums.RESERVA);
    }

    @GetMapping(value = "/reservas/{id}")
    public ResponseDTO getReservasById(@PathVariable Long id){
        return responseDTO.actionFindByIdResponse(EntitiesEnums.RESERVA,id);
    }

    @PostMapping(value = "/reservas")
    public void addReserva(@RequestBody ReservaDTOAdd reserva){

        responseDTO.actionSaveToResponse(reserva);
    }

    @PutMapping(value = "/reservas")
    public void updateReserva(@RequestBody Reserva reserva){
        responseDTO.actionUpdateToResponse(reserva);
    }

    @GetMapping(value = "/reservas/{idUnidad}/{fechaCheckIn}/{rutPersona}")
    public ResponseDTO getReservaDTOBy(@PathVariable Long idUnidad,@PathVariable String fechaCheckIn,@PathVariable Long rutPersona) throws ParseException {
        ResponseDTO responseDTO = new ResponseDTO();
        Reserva reserva = reservaRepository.findReservaByIdUnidadAndFechaIngresoAndRutPersona(idUnidad,new SimpleDateFormat("yyyy-MM-dd").parse(fechaCheckIn)  ,rutPersona);
        responseDTO.setEntidad(reserva);
        return responseDTO;
    }

    @GetMapping(value = "/reservas/rut/{rutPersona}")
    public ResponseDTO getAllReservasByRutPersona(@PathVariable Long rutPersona){
        return responseDTO.actionGetAllToResponseByIdGenerico(EntitiesEnums.RESERVA, rutPersona);
    }

}
