package com.duoc.turimo.app.pl.controllers;

import com.duoc.turimo.app.emuns.EntitiesEnums;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


import com.duoc.turimo.app.bt.IResponseDTO;
import com.duoc.turimo.app.dl.model.PersonaDTO;

import java.util.List;

@RestController
public class PersonaController {

	IResponseDTO personaService;

	@Autowired
	public PersonaController(@Qualifier("responseDTOImpl") IResponseDTO injectedBean) {
		this.personaService = injectedBean;
	}

	@PostMapping(value = "/personas")
	public void addPersona(@RequestBody PersonaDTO persona) {
		personaService.actionSaveToResponse(persona);
	}

	@GetMapping(value = "/personas/{personaId}")
	public PersonaDTO getPersonaById(@PathVariable Long personaId) {
		 return (PersonaDTO) personaService.actionFindByIdResponse(EntitiesEnums.PERSONA,personaId).getEntidad();
	}

	@GetMapping(value = "/personas")
	public List<PersonaDTO> getAllPersonas() {
		return (List<PersonaDTO>) personaService.actionGetAllToResponse(EntitiesEnums.PERSONA).getEntidad();
	}
	

	
	

}
