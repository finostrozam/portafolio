package com.duoc.turimo.app.pl.controllers;

import com.duoc.turimo.app.bt.IPdfService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class PDFController {

    @Autowired
    IPdfService pdfService;

    @GetMapping(value = "/getPDF/{idReserva}",produces = MediaType.APPLICATION_PDF_VALUE)
    public @ResponseBody
    ResponseEntity<byte[]> getActaReservaCheckIn(@PathVariable Long idReserva){

        ResponseEntity<byte[]> response = null;
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_PDF);
            String filename = "output.pdf";
            headers.setContentDispositionFormData(filename, filename);
            headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
            response = new ResponseEntity<>(pdfService.createCheckInPDFFromIdReserva(idReserva), headers, HttpStatus.OK);

        } catch (Exception e) {
            log.error("No se puede construir el PDF.");
        }

        return  response;
    }

    @GetMapping(value = "/getPDF/{idReserva}/{nombreArticulo}/{valor}",produces = MediaType.APPLICATION_PDF_VALUE)
    public @ResponseBody
    ResponseEntity<byte[]> getActaReservaCheckOut(@PathVariable Long idReserva,@PathVariable String nombreArticulo,@PathVariable int valor){

        ResponseEntity<byte[]> response = null;
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_PDF);
            String filename = "output.pdf";
            headers.setContentDispositionFormData(filename, filename);
            headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
            response = new ResponseEntity<>(pdfService.createCheckOutPDFFromIdReserva(idReserva,nombreArticulo,valor), headers, HttpStatus.OK);

        } catch (Exception e) {
            log.error("No se puede construir el PDF.");
        }

        return  response;
    }

    @GetMapping(value = "/getPDF/reporte/",produces = MediaType.APPLICATION_PDF_VALUE)
    public @ResponseBody
    ResponseEntity<byte[]> getReporte(){

        ResponseEntity<byte[]> response = null;
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_PDF);
            String filename = "output.pdf";
            headers.setContentDispositionFormData(filename, filename);
            headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
          //  response = new ResponseEntity<>(pdfService.createReportPDF(), headers, HttpStatus.OK);

        } catch (Exception e) {
            log.error("No se puede construir el PDF.");
        }

        return  response;
    }

}
