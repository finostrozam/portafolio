package com.duoc.turimo.app.pl.controllers;

import com.duoc.turimo.app.bt.IResponseDTO;
import com.duoc.turimo.app.dl.model.PersonaDTO;
import com.duoc.turimo.app.dl.model.ResponseDTO;
import com.duoc.turimo.app.dl.model.UsuarioDTO;
import com.duoc.turimo.app.dl.model.UsuarioDTOAdd;
import com.duoc.turimo.app.emuns.EntitiesEnums;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UsuarioController {


	@Qualifier("responseDTOImpl")
	@Autowired
	IResponseDTO responseDTO;

	@PostMapping(value = "/usuarios")
	public void addUsuario(@RequestBody UsuarioDTOAdd usuario) {

		responseDTO.actionSaveToResponse(usuario);
	}

	@GetMapping(value = "/usuarios/{usuarioId}")
	public ResponseDTO getUsuarioById(@PathVariable Long usuarioId) {
		 return responseDTO.actionFindByIdResponse(EntitiesEnums.USUARIO,usuarioId);
	}

	@GetMapping(value = "/usuarios/nombre/{nombre}")
	public ResponseDTO getUsuarioByUserName(@PathVariable String nombre) {
		return responseDTO.actionFindByIdName(EntitiesEnums.USUARIO,nombre);
	}

	@GetMapping(value = "/usuarios")
	public ResponseDTO getAllUsuarios() {

		return responseDTO.actionGetAllToResponse(EntitiesEnums.USUARIO);
	}



	
	

}
