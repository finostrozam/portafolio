package com.duoc.turimo.app.pl.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HallController {

	@GetMapping("/")
	public String welcomeAll() {
		return "<h1>Bien Venido</h1>";
	}
	
	@GetMapping("/client")
	public String welcomeClient() {
		return "<h1>Bien Venido Cliente</h1>";
	}
	
	@GetMapping("/employee")
	public String welcomeEmployee() {
		return "<h1>Bien Venido Colaborador</h1>";
	}
	
	@GetMapping("/admin")
	public String welcomeAdmin() {
		return "<h1>Bien Venido Administrador</h1>";
	}
}
