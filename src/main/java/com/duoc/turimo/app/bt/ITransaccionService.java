package com.duoc.turimo.app.bt;

import com.duoc.turimo.app.dl.model.Transaccion;

import java.util.List;

public interface ITransaccionService {

	public boolean addTransaccion(Transaccion transaccion);
	
	public Transaccion findTransaccionById(Long idtransaccion);
	
	public List<Transaccion> getAllTransaccions();
	
	public boolean updateStatusTransaccion(Transaccion transaccion);
}
