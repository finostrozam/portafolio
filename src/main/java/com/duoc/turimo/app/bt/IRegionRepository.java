package com.duoc.turimo.app.bt;

import com.duoc.turimo.app.dl.model.Pais;
import com.duoc.turimo.app.dl.model.Region;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IRegionRepository extends CrudRepository<Region,Long> {



}
