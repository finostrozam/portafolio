package com.duoc.turimo.app.bt;

import com.duoc.turimo.app.dl.model.Pasajero;
import com.duoc.turimo.app.dl.model.ServicioDetalle;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface IServicioDetalleRepostory extends CrudRepository<ServicioDetalle,Long> {

    public List<ServicioDetalle> getAllByIdReserva(Long id);


    ServicioDetalle findByIdReserva(Long idServiceDetalle);
}
