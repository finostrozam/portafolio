package com.duoc.turimo.app.bt;

import com.duoc.turimo.app.dl.model.Pais;

import java.util.List;

public interface IPaisService {

    Pais getPais(Long id);

    List<Pais> getAllPaises();
}
