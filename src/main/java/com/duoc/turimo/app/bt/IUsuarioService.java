package com.duoc.turimo.app.bt;

import com.duoc.turimo.app.dl.model.PersonaDTO;
import com.duoc.turimo.app.dl.model.Usuario;
import com.duoc.turimo.app.dl.model.UsuarioDTO;
import com.duoc.turimo.app.dl.model.UsuarioDTOAdd;

import java.util.List;

public interface IUsuarioService {

	void addUsuario(UsuarioDTOAdd usuario);
	
	UsuarioDTO findUsuarioById(Long idUsuario);
	
	List<UsuarioDTO> getAllUsuarios();
	
	void updateStatusUsuario(UsuarioDTO persona);

	void updateUsuario(UsuarioDTO persona);

	boolean exist(UsuarioDTO persona);

	UsuarioDTO findUserByNombreUsuario(String userName);

}
