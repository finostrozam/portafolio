package com.duoc.turimo.app.bt;

import com.duoc.turimo.app.dl.model.ServicioDetalle;
import com.duoc.turimo.app.dl.model.ServicioDetalleAdd;

import java.util.List;

public interface IServicioDetalle {

    public void addServiceDetalle(ServicioDetalleAdd serviceDetalle);

    public ServicioDetalle getServiceDetalleById(Long idServiceDetalle);

    public ServicioDetalle getServiceDetalleByIdReserva(Long idReserva);

    public List<ServicioDetalle> getAllServiceDetalleByIdReserva(Long idReserva);

    public List<ServicioDetalle> getAllServiciosDetalles();

    public int getTotalQtyServiceByReserva(Long idReserva);


}
