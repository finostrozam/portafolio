package com.duoc.turimo.app.bt;

import com.duoc.turimo.app.dl.model.Rol;
import com.duoc.turimo.app.dl.model.UsuarioDTO;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


public interface IRolService{

    void addRol(Rol rol);

    Rol findRolById(Long idRol);

    List<Rol> getAllRoles();


}
