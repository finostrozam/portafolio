package com.duoc.turimo.app.bt;

import com.duoc.turimo.app.dl.model.Servicio;
import com.duoc.turimo.app.dl.model.ServicioDetalle;
import com.duoc.turimo.app.dl.model.UnidadDetalle;
import com.duoc.turimo.app.dl.model.Usuario;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IServicioRepository extends CrudRepository<Servicio, Long> {

    @Query(value = "SELECT * FROM detalles_reserva WHERE id_reserva = ?1", nativeQuery = true)
    ServicioDetalle findByIdReserva(Long idReserva);
}
