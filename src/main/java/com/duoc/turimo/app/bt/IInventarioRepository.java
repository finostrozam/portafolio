package com.duoc.turimo.app.bt;

import com.duoc.turimo.app.dl.model.Inventario;
import com.duoc.turimo.app.dl.model.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IInventarioRepository extends JpaRepository<Inventario,Long> {

    @Query(value = "SELECT * FROM INVENTARIO WHERE ID_UNIDAD_FK = ?1", nativeQuery = true)
    List<Inventario> findAllByIdUnidadFk(Long idUnidadFk);

    @Query(value = "SELECT * FROM INVENTARIO WHERE ID_UNIDAD_FK = ?1", nativeQuery = true)
    Inventario findByIdUnidad(Long idUnidad);
}
