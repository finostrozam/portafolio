package com.duoc.turimo.app.bt;

import com.duoc.turimo.app.dl.model.UnidadDTO;
import com.duoc.turimo.app.dl.model.UnidadDTOAdd;

import java.util.List;


public interface IUnidadService {

	public void addUnidad(UnidadDTOAdd unidad);
	
	public UnidadDTO findUnidadById(Long idUnidad);
	
	public List<UnidadDTO> getAllUnidades();

	public void actualizaUnidad(UnidadDTO unidadDTO);

}
