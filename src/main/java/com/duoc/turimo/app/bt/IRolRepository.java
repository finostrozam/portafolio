package com.duoc.turimo.app.bt;

import com.duoc.turimo.app.dl.model.Rol;
import com.duoc.turimo.app.dl.model.Usuario;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IRolRepository extends CrudRepository<Rol, Long> {

}
