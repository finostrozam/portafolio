package com.duoc.turimo.app.bt;

public interface IParrafoFactory {
    String generarContenido (String parrafo);
}
