package com.duoc.turimo.app.bt;

import com.duoc.turimo.app.dl.model.Persona;
import com.duoc.turimo.app.dl.model.Usuario;
import com.duoc.turimo.app.dl.model.UsuarioDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IUsuarioRepository extends JpaRepository<Usuario, Long> {

    @Query(value = "SELECT * FROM USUARIOS WHERE NOMBRE_USUARIO = ?1", nativeQuery = true)
    Usuario findByNombreUsuario(String nombreUsuario);
}
