package com.duoc.turimo.app.bt;

import com.duoc.turimo.app.dl.model.Pais;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IPaisRepository  extends CrudRepository<Pais,Long> {



}
