package com.duoc.turimo.app.bt;
public interface IPdfService {

    byte[] createCheckInPDFFromIdReserva(Long idReserva) throws Exception;

    byte[] createCheckOutPDFFromIdReserva(Long idReserva,String nombreArticulo,int valor) throws Exception;

   // byte[] createReportPDF() throws Exception;


}
