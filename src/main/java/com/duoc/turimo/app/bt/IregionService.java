package com.duoc.turimo.app.bt;

import com.duoc.turimo.app.dl.model.Region;

import java.util.List;

public interface IregionService {

    public Region getRegionById(Long id);

    public List<Region> getAllRegiones();
}
