package com.duoc.turimo.app.bt;

import com.duoc.turimo.app.dl.model.Reserva;
import com.duoc.turimo.app.dl.model.Usuario;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface IReservaRepository extends CrudRepository<Reserva, Long> {

    @Query(value = "SELECT * FROM RESERVAS WHERE ID_UNIDAD = ?1 AND FECHA_INGRESO =?2 AND RUT_PERSONA=?3", nativeQuery = true)
    public Reserva findReservaByIdUnidadAndFechaIngresoAndRutPersona(Long idUnidad, Date fechaIngreso, Long rutPersona);

    @Query(value = "SELECT * FROM RESERVAS WHERE RUT_PERSONA = ?1", nativeQuery = true)
    public List<Reserva> findAllByRutPersona(Long rutPersona);

    //@Query(value = "select * from reservas where fecha_ingreso = ?1", nativeQuery = true)

}
