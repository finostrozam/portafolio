package com.duoc.turimo.app.bt;

import com.duoc.turimo.app.dl.model.ResponseDTO;
import com.duoc.turimo.app.emuns.EntitiesEnums;
import com.duoc.turimo.app.emuns.TransactionEnums;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.List;


public interface IResponseDTO{

	void actionSaveToResponse(Object object);

    ResponseDTO actionGetAllToResponse(EntitiesEnums contextoEnum);

    ResponseDTO actionFindByIdResponse(EntitiesEnums contextoEnum, Long id);

	ResponseDTO actionFindByIdName(EntitiesEnums contextoEnum, String name);

	void actionUpdateToResponse(Object object);

	boolean isPresent(Object object);

	ResponseDTO<List> actionGetAllToResponseByIdGenerico(EntitiesEnums contextsEnum,Long rutPersona);
}
