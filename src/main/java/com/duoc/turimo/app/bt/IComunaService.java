package com.duoc.turimo.app.bt;

import com.duoc.turimo.app.dl.model.Comuna;

import java.util.List;

public interface IComunaService {

    public Comuna getComunaById(Long id);

    public List<Comuna> getAllComunas();
}
