package com.duoc.turimo.app.bt;

public interface EmailService {
    void sendSimpleMessage(String to, String subject, String text);

    void enviarCorreos();
}
