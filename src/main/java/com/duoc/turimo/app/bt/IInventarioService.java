package com.duoc.turimo.app.bt;

import com.duoc.turimo.app.dl.model.Inventario;
import com.duoc.turimo.app.dl.model.InventarioAdd;

import java.util.List;

public interface IInventarioService {

    public List<Inventario> findAll();

    public String detailAll(Long id);

    public void addInventarioToUnidad(InventarioAdd inventarioAdd);

    Inventario findInventarioByIdUnidad(Long idUnidad);

    List<Inventario> findAllInventariosByIdUnidad(Long idUnidad);
}
