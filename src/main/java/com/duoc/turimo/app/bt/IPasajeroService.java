package com.duoc.turimo.app.bt;

import com.duoc.turimo.app.dl.model.Pasajero;
import com.duoc.turimo.app.dl.model.PasajeroDTO;
import com.duoc.turimo.app.dl.model.PasajeroDTOAdd;

import java.util.List;

public interface IPasajeroService {

	public void addPasajero(PasajeroDTOAdd pasajeroDTO);
	
	public PasajeroDTO findPasajeroById(Long idpasajero);
	
	public List<PasajeroDTO> getAllPasajeros();

	public List<Pasajero> getAllPasajerosByIdReserva(Long idReserva);
	
	public void updateStatusPasajero(PasajeroDTO pasajero);
}
