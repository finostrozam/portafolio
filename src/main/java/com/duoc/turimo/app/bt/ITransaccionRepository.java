package com.duoc.turimo.app.bt;

import com.duoc.turimo.app.dl.model.Transaccion;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ITransaccionRepository extends CrudRepository<Transaccion, Long> {

}
