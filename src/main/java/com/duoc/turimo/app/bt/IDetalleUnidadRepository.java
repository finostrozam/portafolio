package com.duoc.turimo.app.bt;

import com.duoc.turimo.app.dl.model.UnidadDetalle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface IDetalleUnidadRepository extends JpaRepository<UnidadDetalle,Long> {

    @Query(value = "select * from UNIDAD_DETALLE WHERE  ID_UNIDAD_FK = ?1", nativeQuery = true)
    List<UnidadDetalle> findAllByIdUnidad(Long idUnidad);
}
