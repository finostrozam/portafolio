package com.duoc.turimo.app.bt;

import com.duoc.turimo.app.dl.model.Unidad;
import com.duoc.turimo.app.dl.model.UnidadDetalle;
import com.duoc.turimo.app.dl.model.UnidadDetalleAdd;

import java.util.List;

public interface IDetalleUnidadService {

    void saveImagenUnidad(UnidadDetalleAdd unidadDetalleAdd);

    List<UnidadDetalle> getAllImagesByIdUnidad(Long idUnidad);

    void saveAllImages(List<UnidadDetalleAdd> detallesUnidades);
}
