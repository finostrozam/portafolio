package com.duoc.turimo.app.bt;

import java.util.List;

import com.duoc.turimo.app.dl.model.PersonaDTO;

public interface IPersonaService {

	void addPersona(PersonaDTO persona);
	
	PersonaDTO findPersonaById(Long idPersona);
	
	List<PersonaDTO> getAllPersonas();
	
	void updateStatusPersona(PersonaDTO persona);

	boolean exist(PersonaDTO persona);
}
