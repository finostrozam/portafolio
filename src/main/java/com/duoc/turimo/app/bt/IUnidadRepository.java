package com.duoc.turimo.app.bt;

import com.duoc.turimo.app.dl.model.Unidad;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IUnidadRepository extends JpaRepository<Unidad, Long> {
}
