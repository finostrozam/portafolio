package com.duoc.turimo.app.bt;

import com.duoc.turimo.app.dl.model.Pasajero;
import com.duoc.turimo.app.dl.model.Reserva;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IPasajeroRepository extends CrudRepository<Pasajero, Long> {

    @Query(value = "select * from pasajeros where id_reserva_fk = ?1 and id_unidad = ?2",nativeQuery = true)
    public List<Pasajero> findAllByIdReservaAndIdUnidad(Long idReserva,Long idUnidad);

    @Query(value = "select * from pasajeros where id_reserva_fk = ?1",nativeQuery = true)
    public List<Pasajero> findAllByIdReserva(Long idReservaFk);


}
