package com.duoc.turimo.app.bt;

import com.duoc.turimo.app.dl.model.Comuna;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IComunaRepository extends CrudRepository <Comuna,Long> {
}
