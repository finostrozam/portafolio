package com.duoc.turimo.app.bt;

import com.duoc.turimo.app.dl.model.Proveedor;
import com.duoc.turimo.app.dl.model.ProveedorAdd;

import java.util.List;

public interface IProveedorService {
    public Proveedor getProveedorById(Long id);
    public List<Proveedor> getAllProveedores();
    public void addProveedor(ProveedorAdd proveedor);
    public void aupdateProveedor(Proveedor proveedor);
}
