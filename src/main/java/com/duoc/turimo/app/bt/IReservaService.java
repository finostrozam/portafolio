package com.duoc.turimo.app.bt;

import com.duoc.turimo.app.dl.model.Reserva;
import com.duoc.turimo.app.dl.model.ReservaDTOAdd;

import java.util.List;

public interface IReservaService {

	public void addReserva(ReservaDTOAdd reserva);
	
	public Reserva findReservaById(Long idReserva);
	
	public List<Reserva> getAllReservas();

	public List<Reserva> getAllReservasByRutPersona(Long rutPersona);
	
	public void updateStatusReserva(Reserva reserva);
}
