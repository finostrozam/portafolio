package com.duoc.turimo.app.bt;

import com.duoc.turimo.app.dl.model.Proveedor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IProveedorRepository extends CrudRepository<Proveedor,Long> {

}
