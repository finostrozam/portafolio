package com.duoc.turimo.app.bt;

import com.duoc.turimo.app.dl.model.ServicioDTO;

import java.util.List;

public interface IServicioService {

	public void addServicio(ServicioDTO servicio);
	
	public ServicioDTO findServicioById(Long idservicio);

	public List<ServicioDTO> getAllServicios();
	
	public void updateStatusServicio(ServicioDTO servicio);
}
