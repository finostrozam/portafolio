package com.duoc.turimo.app.emuns;

public enum TransactionEnums {
	
	UPDATE("UPDATE OBJECT"), 
    GETALL("GET ALL OBJECTS"), 
    FIND("FIND OBJECT BY ID"), 
    ADD("ADD AN OBJECT");
 
    private String transaction;
 
    TransactionEnums(String transaction) {
        this.transaction = transaction;
    }
 
    public String getStringValue() {
        return transaction;
    }



}
