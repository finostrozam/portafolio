package com.duoc.turimo.app.emuns;

public enum Errores {

    NULL("No puede solicitar informacion con el campo en null"),
    ENTIDAD_NO_EXISTE("No se encuentran registros para esta buesqueda"),
    FECHAS_ERRONEAS_ORDEN("No puede designar la fecha de Checkin posterior a la de checkout."),
    FECHAS_ERRONEAS_CHECKOUT("No puede designar la fecha de Checkout anterior a hoy."),
    FECHAS_ERRONEAS_CHECKIN("No puede designar la fecha de Checkin anterior a hoy."),
    FECHAS_ERRONEAS_ANTES_CHECKIN("Solo puede ejecutar esta operacion un día antes del Checkin"),
    UNIDAD_NO_DISPONIBLE("La unidad no se encuentra disponible.");

    private String transaction;

    Errores(String transaction) {
        this.transaction = transaction;
    }

    public String getStringValue() {
        return transaction;
    }
}
