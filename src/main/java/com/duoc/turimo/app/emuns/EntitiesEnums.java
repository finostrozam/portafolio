package com.duoc.turimo.app.emuns;

public enum EntitiesEnums {

	PERSONA("PERSONA"),
    UNIDAD("UNIDAD"),
    INVENTARIO("INVENTARIO"),
    UNIDAD_DETALLE("UNIDAD_DETALLE"),
    RESERVA("RESERVA"),
    SERVICIO("SERVICIO"),
    SERVICIO_DETALLE("SERVICIO_DETALLE"),
    PASAJERO("PASAJERO"),
    USUARIO("USUARIO"),
    COMUNA("COMUNA"),
    REGION("REGION"),
    PROVEEDOR("PROVEEDOR"),
    ROL("ROL"),
    PAIS("PAIS");


    private String entity;

    EntitiesEnums(String transaction) {
        this.entity = transaction;
    }

    public String getStringValue() {
        return entity;
    }



}
