package com.duoc.turimo.app.bl.services;

import com.duoc.turimo.app.bt.*;
import com.duoc.turimo.app.dl.model.*;
import com.duoc.turimo.app.utils.CalculatorUtil;
import com.duoc.turimo.app.utils.Validator;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.event.internal.DefaultPersistOnFlushEventListener;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class ServicioDetalleImpl implements IServicioDetalle {

    private final Long SERVICIO_TRANSPORTE = 9L;

    @Autowired
    private IServicioDetalleRepostory servicioDetalleRepostory;

    @Autowired
    ModelMapper modelMapper;

    @Autowired
    IReservaRepository reservaRepository;

    @Autowired
    IServicioRepository servicioRepository;

    @Autowired
    IPersonaRepository personaRepository;

    @Autowired
    IUnidadRepository unidadRepositoryRepository;

    @Qualifier("getJavaMailSender")
    @Autowired
    private JavaMailSender emailSender;


    @Override
    @Transactional
    public void addServiceDetalle(ServicioDetalleAdd serviceDetalleAdd) {

        ServicioDetalle servicioDetalle = new ServicioDetalle();
        servicioDetalle.setIdReserva(serviceDetalleAdd.getIdReserva());
        servicioDetalle.setIdServicio(serviceDetalleAdd.getIdServicio());
        servicioDetalle.setQty(serviceDetalleAdd.getQty());
        Servicio servicio = servicioRepository.findById(serviceDetalleAdd.getIdServicio()).orElse(null);
        servicioDetalle.setPrecioUnitario(Validator.isIntegerNullSoZero(servicio.getMontoServicio()));
        servicioDetalle.setTotal(Validator.isIntegerNullSoZero(servicioDetalle.getQty()) * Validator.isIntegerNullSoZero(servicioDetalle.getPrecioUnitario()) );
        servicioDetalleRepostory.save(servicioDetalle);

        Reserva reserva = reservaRepository.findById(serviceDetalleAdd.getIdReserva()).orElse(null);
        List<ServicioDetalle> allDetallesReservasByIdReserva = servicioDetalleRepostory.getAllByIdReserva(serviceDetalleAdd.getIdReserva());
        reserva.setCantServicios(CalculatorUtil.getSumQtyForEachElementOnServicioDetalle(allDetallesReservasByIdReserva));
        reserva.setSubTotalServicios(CalculatorUtil.getSumTotalForEachElementOnServicioDetalle(allDetallesReservasByIdReserva));
        reserva.setMontoTotalReserva(Validator.isIntegerNullSoZero(reserva.getSubTotalDias()) + Validator.isIntegerNullSoZero(reserva.getSubTotalServicios()) );
        reservaRepository.save(reserva);

        if(serviceDetalleAdd.getIdServicio() == SERVICIO_TRANSPORTE){
            enviaCorreoDeTransporte(reserva);
        }

    }

    public void sendSimpleMessage(String to, String subject, String text) {

        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("turismo.real.duoc.2020@gmail.com");
        message.setTo(to);
        message.setSubject(subject);
        message.setText(text);
        emailSender.send(message);

    }

    public void enviaCorreoDeTransporte(Reserva reserva){
        Optional<Persona> persona = personaRepository.findById(reserva.getRutPersona());
        Optional<Unidad> unidad = unidadRepositoryRepository.findById(reserva.getIdUnidad());
        if(persona.isPresent() && unidad.isPresent()){
            String cuerpoMensaje = String.format("Recuerda %s que el dia %s a las 12:15 paará por ti Rodrigo Maturana para llavarte desde : %s , hasta: %s  ",
                    persona.get().getNombrePersona(),
                    reserva.getFechaSalida(),
                    unidad.get().getDireccion(),
                    "Aeropuerto nacional");
            sendSimpleMessage(persona.get().getCorreoPersona(),"Correo de Transporte",cuerpoMensaje);
        }else{
            log.error("No se encuentra a la persona  para enviar el aviso de transporte. rut persona");
        }

    }

    @Override
    public ServicioDetalle getServiceDetalleById(Long idServiceDetalle) {
        return servicioDetalleRepostory.findById(idServiceDetalle).orElse(null);

    }

    @Override
    public ServicioDetalle getServiceDetalleByIdReserva(Long idReserva) {
        return servicioDetalleRepostory.findByIdReserva(idReserva);

    }

    @Override
    public List<ServicioDetalle> getAllServiceDetalleByIdReserva(Long idReserva) {
        return servicioDetalleRepostory.getAllByIdReserva(idReserva);

    }

    @Override
    public List<ServicioDetalle> getAllServiciosDetalles() {
        return (List<ServicioDetalle>) servicioDetalleRepostory.findAll();

    }

    @Override
    public int getTotalQtyServiceByReserva(Long idReserva) {
        return getAllServiceDetalleByIdReserva(idReserva).size();

    }
}
