package com.duoc.turimo.app.bl.services;

import com.duoc.turimo.app.bt.IComunaRepository;
import com.duoc.turimo.app.bt.IComunaService;
import com.duoc.turimo.app.dl.model.Comuna;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ComunaServiceImpl implements IComunaService {

    @Autowired
    IComunaRepository comunaRepository;

    @Override
    public Comuna getComunaById(Long id) {
        return comunaRepository.findById(id).orElse(null);
    }

    @Override
    public List<Comuna> getAllComunas() {
        return (List<Comuna>) comunaRepository.findAll();
    }
}
