package com.duoc.turimo.app.bl.services;

import com.duoc.turimo.app.bt.*;
import com.duoc.turimo.app.dl.model.*;
import com.duoc.turimo.app.emuns.EntitiesEnums;
import com.duoc.turimo.app.utils.DateUtil;

import com.duoc.turimo.app.utils.Validator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.duoc.turimo.app.emuns.TransactionEnums;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;


@Service
@Slf4j
public class ResponseDTOImpl implements IResponseDTO {

	@Autowired
	ITransaccionRepository transaccionRepository;
	
	@Autowired
	IPersonaService personaService;
	
	@Autowired
	IUnidadService unidadService;
	
	@Autowired
	IReservaService reservaService;
	
	@Autowired
	IServicioService servicioService;
	
	@Autowired
	IPasajeroService pasajeroService;

	@Autowired
	IUsuarioService usuarioService;

	@Autowired
	IRolService rolService;

	@Autowired
	IPaisService paisService;

	@Autowired
	IregionService regionService;

	@Autowired
	IComunaService comunaService;

	@Autowired
	IProveedorService proveedorService;

	@Autowired
	IServicioDetalle servicioDetalle;

	@Autowired
	IDetalleUnidadService detalleUnidadService;

	@Autowired
	IInventarioService inventarioService;


	@Override
	public void actionSaveToResponse(Object object) throws ResponseStatusException {
		Transaccion transaccion = new Transaccion();
		if (object != null) {
			
			transaccion.setTipoTransaccion(TransactionEnums.ADD.getStringValue());
			transaccion.setEstadoTransaccion("PENDIENTE");

				
				if (object instanceof PersonaDTO) {
					transaccion.setMensajeTransaccion("Ingreso Persona");
					PersonaDTO personaDTO = (PersonaDTO) object;
					if(personaService.exist(personaDTO)){
						transaccion.setMensajeTransaccion("Actualización Persona");
						transaccion.setTipoTransaccion(TransactionEnums.UPDATE.getStringValue());
					}
					personaService.addPersona(personaDTO);

				}else if (object instanceof UnidadDTOAdd) {
					transaccion.setMensajeTransaccion("Ingreso Unidad");
					unidadService.addUnidad((UnidadDTOAdd) object);
				}else if (object instanceof UnidadDetalleAdd[]) {
					transaccion.setMensajeTransaccion("Ingreso de Detalles de Unidades");
					List<UnidadDetalleAdd> unidadDetalleAdds = Arrays.asList( (UnidadDetalleAdd) object);
					detalleUnidadService.saveAllImages((unidadDetalleAdds));
				}else if (object instanceof UnidadDetalleAdd) {
						transaccion.setMensajeTransaccion("Guardar imagen");
						detalleUnidadService.saveImagenUnidad((UnidadDetalleAdd) object);
				}else if (object instanceof UsuarioDTOAdd) {
					transaccion.setMensajeTransaccion("Ingreso Unidad");
					usuarioService.addUsuario((UsuarioDTOAdd) object);
				}else if (object instanceof InventarioAdd) {
					transaccion.setMensajeTransaccion("Ingreso de inventario");
					inventarioService.addInventarioToUnidad((InventarioAdd) object);
				}
				else if (object instanceof ReservaDTOAdd) {
					transaccion.setMensajeTransaccion("Ingreso Reserva");
					ReservaDTOAdd reservaDTOAdd = (ReservaDTOAdd) object;
					Validator.nullValidation(object);
					Validator.checkInDateValidation(reservaDTOAdd.getFechaIngreso());
					Validator.checkOutDateValidation(reservaDTOAdd.getFechaSalida());
					Validator.checkDatesOrder(reservaDTOAdd.getFechaIngreso(),reservaDTOAdd.getFechaSalida());
					reservaService.addReserva(reservaDTOAdd);

				}else if (object instanceof PasajeroDTOAdd) {
					transaccion.setMensajeTransaccion("Ingreso Pasajero");
					PasajeroDTOAdd pasajeroDTOAdd = (PasajeroDTOAdd) object;
					Validator.nullValidation(object);
					Validator.obligatorioValidation(pasajeroDTOAdd.getRutPasajero());
					Validator.obligatorioValidation(String.valueOf(pasajeroDTOAdd.getIdUnidad()));
					Validator.obligatorioValidation(String.valueOf(pasajeroDTOAdd.getIdReserva()));
					pasajeroService.addPasajero(pasajeroDTOAdd);

				}else if (object instanceof ServicioDetalleAdd) {
				transaccion.setMensajeTransaccion("Ingreso Servicio");
				ServicioDetalleAdd servicioDetalleAdd = (ServicioDetalleAdd) object;
				Validator.nullValidation(servicioDetalleAdd);
				Validator.obligatorioValidation( String.valueOf(servicioDetalleAdd.getIdReserva()));
				Validator.obligatorioValidation( String.valueOf(servicioDetalleAdd.getIdServicio()));
				servicioDetalle.addServiceDetalle(servicioDetalleAdd);

				}
				else if (object instanceof ServicioDTO) {
					transaccion.setMensajeTransaccion("Ingreso Servicio");
					servicioService.addServicio((ServicioDTO) object);

				} else if (object instanceof ProveedorAdd) {
					transaccion.setMensajeTransaccion("Ingreso Pasajero");
					proveedorService.addProveedor((ProveedorAdd) object);

				}

		}else {
			transaccion.setMensajeTransaccion("Error objeto null");
			
		}
		transaccion.setEstadoTransaccion(TransactionEnums.ADD.getStringValue());
		transaccionRepository.save(transaccion);

	}

	@Override
	public ResponseDTO<List> actionGetAllToResponse(EntitiesEnums contextsEnum) {
		Transaccion transaccion = new Transaccion();
		ResponseDTO<List> respuesta = new ResponseDTO<>();

		if (contextsEnum != null) {

			transaccion.setTipoTransaccion(TransactionEnums.GETALL.getStringValue());
			transaccion.setEstadoTransaccion("PENDIENTE");
			try {

				if (contextsEnum == EntitiesEnums.PERSONA) {
					transaccion.setMensajeTransaccion("Get All Personas");
					respuesta.setEntidad(personaService.getAllPersonas());

				} else if (contextsEnum == EntitiesEnums.UNIDAD) {
					transaccion.setMensajeTransaccion("Get All Unidades");
					respuesta.setEntidad(unidadService.getAllUnidades());

				} else if (contextsEnum == EntitiesEnums.RESERVA) {
					transaccion.setMensajeTransaccion("Get All Reservas");
					respuesta.setEntidad(reservaService.getAllReservas());
				}else if (contextsEnum == EntitiesEnums.INVENTARIO) {
					transaccion.setMensajeTransaccion("Get All Inventarios");
					respuesta.setEntidad(inventarioService.findAll());

				} else if (contextsEnum == EntitiesEnums.SERVICIO) {
					transaccion.setMensajeTransaccion("Get All Servicios");
					respuesta.setEntidad(servicioService.getAllServicios());

				}else if (contextsEnum == EntitiesEnums.SERVICIO_DETALLE) {
					transaccion.setMensajeTransaccion("Get All Servicios");
					respuesta.setEntidad(servicioDetalle.getAllServiciosDetalles());

				} else if (contextsEnum == EntitiesEnums.PASAJERO) {
					transaccion.setMensajeTransaccion("Get All Pasajeros");
					respuesta.setEntidad(pasajeroService.getAllPasajeros());

				}else if (contextsEnum == EntitiesEnums.USUARIO) {
					transaccion.setMensajeTransaccion("Get All Usuarios");
					respuesta.setEntidad(usuarioService.getAllUsuarios());

				}else if (contextsEnum == EntitiesEnums.PAIS) {
				transaccion.setMensajeTransaccion("Get All Paises");
				respuesta.setEntidad(paisService.getAllPaises());

				}else if (contextsEnum == EntitiesEnums.REGION) {
					transaccion.setMensajeTransaccion("Get All Regiones");
					respuesta.setEntidad(regionService.getAllRegiones());

				}else if (contextsEnum == EntitiesEnums.COMUNA) {
					transaccion.setMensajeTransaccion("Get All Comunas");
					respuesta.setEntidad(comunaService.getAllComunas());

				}else if (contextsEnum == EntitiesEnums.PROVEEDOR) {
					transaccion.setMensajeTransaccion("Get All Proveedores");
					respuesta.setEntidad(proveedorService.getAllProveedores());

				}


			}catch(Exception e) {
				transaccion.setMensajeTransaccion("Error durante la obtencion de las entidades");

			}



		}else {
			transaccion.setMensajeTransaccion("Error objeto null");

		}
		transaccion.setEstadoTransaccion(TransactionEnums.ADD.getStringValue());
		transaccionRepository.save(transaccion);
		return  respuesta;
	}

	@Override
	public ResponseDTO<List> actionGetAllToResponseByIdGenerico(EntitiesEnums contextsEnum,Long id) {
		Transaccion transaccion = new Transaccion();
		ResponseDTO<List> respuesta = new ResponseDTO<>();

		if (contextsEnum != null) {



			transaccion.setTipoTransaccion(TransactionEnums.GETALL.getStringValue());
			transaccion.setEstadoTransaccion("PENDIENTE");
			try {

				if (contextsEnum == EntitiesEnums.RESERVA) {
					transaccion.setMensajeTransaccion("Get All Reservas by rut persona");
					List<Reserva> reservas = reservaService.getAllReservasByRutPersona(id);
					respuesta.setEntidad(reservas);

				}else if(contextsEnum == EntitiesEnums.UNIDAD_DETALLE){
					transaccion.setMensajeTransaccion("Get All Unidad detalle by id Unidad");
					List<UnidadDetalle> reservas = detalleUnidadService.getAllImagesByIdUnidad(id);
					respuesta.setEntidad(reservas);

				}else if(contextsEnum == EntitiesEnums.INVENTARIO){
					transaccion.setMensajeTransaccion("Get All Unidad detalle by id Unidad");
					List<Inventario> inventarios = inventarioService.findAllInventariosByIdUnidad(id);
					respuesta.setEntidad(inventarios);

				}else if(contextsEnum == EntitiesEnums.PASAJERO){
					transaccion.setMensajeTransaccion("Get All Unidad detalle by id Unidad");
					List<Pasajero> pasajeros = pasajeroService.getAllPasajerosByIdReserva(id);
					respuesta.setEntidad(pasajeros);

				}else if(contextsEnum == EntitiesEnums.SERVICIO_DETALLE){
					transaccion.setMensajeTransaccion("Get All detalle servicios by id reserva");
					List<ServicioDetalle> servicios = servicioDetalle.getAllServiceDetalleByIdReserva(id);
					respuesta.setEntidad(servicios);

				}


			}catch(Exception e) {
				transaccion.setMensajeTransaccion("Error durante la obtencion de las entidades");

			}



		}else {
			transaccion.setMensajeTransaccion("Error objeto null");

		}
		transaccion.setEstadoTransaccion(TransactionEnums.ADD.getStringValue());
		transaccionRepository.save(transaccion);
		return  respuesta;
	}

	@Override
	public ResponseDTO actionFindByIdResponse(EntitiesEnums contextsEnum, Long id) {
		Transaccion transaccion = new Transaccion();
		ResponseDTO<java.io.Serializable> respuesta = null;

		if (contextsEnum != null) {


			respuesta = new ResponseDTO<>();
			transaccion.setTipoTransaccion(TransactionEnums.FIND.getStringValue());
			transaccion.setEstadoTransaccion("PENDIENTE");
			try {

				if (contextsEnum == EntitiesEnums.PERSONA) {
					transaccion.setMensajeTransaccion("Get Personas por ID");
					respuesta.setEntidad(personaService.findPersonaById(id));

				} else if (contextsEnum == EntitiesEnums.UNIDAD) {
					transaccion.setMensajeTransaccion("Get Unidad por ID");
					respuesta.setEntidad(unidadService.findUnidadById(id));

				} else if (contextsEnum == EntitiesEnums.RESERVA) {
					transaccion.setMensajeTransaccion("Get Reserva por ID");
					respuesta.setEntidad(reservaService.findReservaById(id));

				} else if (contextsEnum == EntitiesEnums.SERVICIO) {
					transaccion.setMensajeTransaccion("Get Servicio por ID");
					respuesta.setEntidad(servicioService.findServicioById(id));

				}else if (contextsEnum == EntitiesEnums.SERVICIO_DETALLE) {
					transaccion.setMensajeTransaccion("Get Servicio por ID");
					respuesta.setEntidad(servicioDetalle.getServiceDetalleByIdReserva(id));

				} else if (contextsEnum == EntitiesEnums.PASAJERO) {
					transaccion.setMensajeTransaccion("Get Pasajero por ID");
					respuesta.setEntidad(pasajeroService.findPasajeroById(id));

				} else if (contextsEnum == EntitiesEnums.INVENTARIO) {
					transaccion.setMensajeTransaccion("Get Invenventario por ID unidad");
					respuesta.setEntidad(inventarioService.findInventarioByIdUnidad(id));

				} else if (contextsEnum == EntitiesEnums.USUARIO) {
					transaccion.setMensajeTransaccion("Get Usuario por ID");
					respuesta.setEntidad(usuarioService.findUsuarioById(id));

				}else if (contextsEnum == EntitiesEnums.PAIS) {
					transaccion.setMensajeTransaccion("Get Usuario por ID");
					respuesta.setEntidad(paisService.getPais(id));

				}else if (contextsEnum == EntitiesEnums.REGION) {
					transaccion.setMensajeTransaccion("Get Region por ID");
					respuesta.setEntidad(regionService.getRegionById(id));

				}else if (contextsEnum == EntitiesEnums.COMUNA) {
					transaccion.setMensajeTransaccion("Get Comuna por ID");

					respuesta.setEntidad(comunaService.getComunaById(id));

				}else if (contextsEnum == EntitiesEnums.PROVEEDOR) {
					transaccion.setMensajeTransaccion("Get Proveedor por ID");
					respuesta.setEntidad(proveedorService.getProveedorById(id));

				}



			}catch(Exception e) {
				transaccion.setMensajeTransaccion("Error durante la obtencion de las entidades");

			}



		}else {
			transaccion.setMensajeTransaccion("Error objeto null");

		}
		transaccion.setEstadoTransaccion(TransactionEnums.ADD.getStringValue());
		transaccion.setFechaTransaccion(DateUtil.getSQLDate("yyyy-MM-dd HH:mm:ss"));
		transaccionRepository.save(transaccion);
		return  respuesta;
	}

	@Override
	public ResponseDTO actionFindByIdName(EntitiesEnums contextsEnum, String name) {
		Transaccion transaccion = new Transaccion();
		ResponseDTO<java.io.Serializable> respuesta = null;

		if (contextsEnum != null) {


			respuesta = new ResponseDTO<>();
			transaccion.setTipoTransaccion(TransactionEnums.FIND.getStringValue());
			transaccion.setEstadoTransaccion("PENDIENTE");


				if (contextsEnum == EntitiesEnums.USUARIO) {
						transaccion.setMensajeTransaccion("Get Usuario por ID");
						respuesta.setEntidad(usuarioService.findUserByNombreUsuario(name));
				}




		}else {
			transaccion.setMensajeTransaccion("Error objeto null");

		}
		transaccion.setEstadoTransaccion(TransactionEnums.ADD.getStringValue());
		transaccion.setFechaTransaccion(DateUtil.getSQLDate("yyyy-MM-dd HH:mm:ss"));
		transaccionRepository.save(transaccion);
		return  respuesta;
	}

	@Override
	public void actionUpdateToResponse(Object object) {
		Transaccion transaccion = new Transaccion();
		log.info("Se inicia proceso de actualizacion.");
		 if (object instanceof UsuarioDTO) {
			transaccion.setMensajeTransaccion("Actualizacion Usuario");
			UsuarioDTO usuarioDTO = (UsuarioDTO) object;
			log.info( String.format("Se inicia proceso de busqueda para la persona con rut [%s].",usuarioDTO.getRutPersona()));
			PersonaDTO persona = personaService.findPersonaById(usuarioDTO.getRutPersona());
			Validator.nullValidation(persona);
			Validator.obligatorioValidation(persona.getNombrePersona());
			Validator.obligatorioValidation(String.valueOf(persona.getRutPersona()));
			Validator.obligatorioValidation(persona.getCorreoPersona());
			log.info( String.format("Se inicia proceso de validacion de ROL para la persona con rut [%s].",usuarioDTO.getRutPersona()));
			Rol rol = rolService.findRolById(usuarioDTO.getIdRol());
			Validator.nullValidation(rol);
			usuarioService.updateUsuario((UsuarioDTO) object);
			transaccion.setMensajeTransaccion("Usuario guardado exitosamente!");

		}else if(object instanceof UnidadDTO){
		 	unidadService.actualizaUnidad((UnidadDTO) object);
		 }else if(object instanceof Reserva){
		 	reservaService.updateStatusReserva((Reserva) object);
		 }else if(object instanceof Proveedor){
			 proveedorService.aupdateProveedor((Proveedor) object);
		 }

		transaccion.setEstadoTransaccion(TransactionEnums.ADD.getStringValue());
		transaccion.setFechaTransaccion(DateUtil.getSQLDate("yyyy-MM-dd HH:mm:ss"));
		transaccionRepository.save(transaccion);

	}

	@Override
	public boolean isPresent(Object object) {
		return false;
	}

}
