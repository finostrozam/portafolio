package com.duoc.turimo.app.bl.services;

import com.duoc.turimo.app.bt.IUsuarioRepository;
import com.duoc.turimo.app.bt.IUsuarioService;
import com.duoc.turimo.app.dl.model.Usuario;
import com.duoc.turimo.app.dl.model.UsuarioDTO;
import com.duoc.turimo.app.dl.model.UsuarioDTOAdd;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.*;


@Service
public class UsuarioServiceImpl implements IUsuarioService {

	@Autowired
	private IUsuarioRepository usuarioRepository;

	@Autowired
	ModelMapper modelMapper;
	
	@Override
	@Transactional
	public void addUsuario(UsuarioDTOAdd usuarioDTOAdd) {
		Usuario usuario = usuarioRepository.findByNombreUsuario(usuarioDTOAdd.getNombreUsuario());
		if(usuario != null){
			usuario.setContrasena(usuarioDTOAdd.getContrasena());
			usuario.setIdRol(usuarioDTOAdd.getIdRol());
			usuario.setEstado(usuarioDTOAdd.getEstado());
			usuarioRepository.save(usuario);
		}else{
			usuarioDTOAdd.setFechaCreacion(Calendar.getInstance().getTime());
			usuarioRepository.save(modelMapper.map(usuarioDTOAdd,Usuario.class));
		}

	}

	@Override
	public UsuarioDTO findUsuarioById(Long idUsuario) {
		UsuarioDTO usuarioDTO = null;
		Usuario usuario  = usuarioRepository.findById(idUsuario).orElse(null);
		if(usuario != null) {

			usuarioDTO = new UsuarioDTO();
			usuarioDTO.setContrasena(usuario.getContrasena());
			usuarioDTO.setEstado(usuario.getEstado());
			usuarioDTO.setFechaCreacion(usuario.getFechaCreacion());
			usuarioDTO.setIdRol(usuario.getIdRol());
			usuarioDTO.setRutPersona(usuario.getRutPersona());
			usuarioDTO.setIdUsario(usuario.getIdUsario());
			usuarioDTO.setNombreUsuario(usuario.getNombreUsuario());
		}
		return usuarioDTO;
	}

	@Override
	public List<UsuarioDTO> getAllUsuarios() {
		List<Usuario> usuario = (List<Usuario>) usuarioRepository.findAll();
		List<UsuarioDTO> usuariosDTO = new ArrayList<>();

		usuario.stream().forEach(p ->  {
			usuariosDTO.add(modelMapper.map(p,UsuarioDTO.class));
		} );
 		return usuariosDTO;
	}

	@Override
	@Transactional
	public void updateStatusUsuario(UsuarioDTO usuarioDTO) {
		
		Optional<Usuario> usuario = usuarioRepository.findById(usuarioDTO.getIdUsario());
		if(usuario.isPresent()) { 
			usuarioRepository.save(usuario.get());
		}else{
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No existe la persona");		}
	}

	@Override
	public void updateUsuario(UsuarioDTO usuarioDTO) {
		Usuario usuario = usuarioRepository.findById(usuarioDTO.getIdUsario()).orElse(null);
		if(usuario==null){
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "El usuario que pretende actualizar no existe");
		}else{
			usuarioRepository.save(modelMapper.map(usuarioDTO,Usuario.class));
		}
	}

	@Override
	public boolean exist(UsuarioDTO usuario) {
		return usuarioRepository.existsById(usuario.getIdUsario());
	}

	@Override
	public UsuarioDTO findUserByNombreUsuario(String userName) {
		Usuario usuario = usuarioRepository.findByNombreUsuario(userName);
			return  modelMapper.map(usuario,UsuarioDTO.class);
	}


}
