package com.duoc.turimo.app.bl.services;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import com.duoc.turimo.app.bt.IUnidadRepository;
import com.duoc.turimo.app.dl.model.Reserva;
import com.duoc.turimo.app.dl.model.ReservaDTOAdd;
import com.duoc.turimo.app.dl.model.Unidad;
import com.duoc.turimo.app.utils.DateUtil;
import com.duoc.turimo.app.utils.Validator;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.duoc.turimo.app.bt.IReservaRepository;
import com.duoc.turimo.app.bt.IReservaService;

@Service
@Slf4j
public class ReservaServiceImpl implements IReservaService {

	@Autowired
	private IReservaRepository reservaRepository;

	@Autowired
	private IUnidadRepository unidadRepository;

	@Autowired
	private ModelMapper modelMapper;

	@Override
	@Transactional
	public void addReserva(ReservaDTOAdd reservaDTO) {
		Unidad unidad = unidadRepository.findById(reservaDTO.getIdUnidad()).orElse(null);

		if (unidad !=null && unidad.getDisponible()){
			int diff = DateUtil.qtyOfBetweenDays(reservaDTO.getFechaSalida(),reservaDTO.getFechaIngreso());
			Integer totalDiasPorPrecioUnidad = diff * unidad.getPrecio();
			Reserva reserva = new Reserva();
			reserva.setRutPersona(reservaDTO.getRutPersona());
			reserva.setAbono(reservaDTO.getAbono());
			reserva.setIdUnidad(reservaDTO.getIdUnidad());
			reserva.setFechaIngreso(DateUtil.getJavaUtilDate(reservaDTO.getFechaIngreso(),"yyyy-MM-dd"));
			reserva.setFechaSalida(DateUtil.getJavaUtilDate(reservaDTO.getFechaSalida(),"yyyy-MM-dd") );
			reserva.setAvisoCheckInEnviado(false);
			reserva.setSubTotalDias(Validator.isIntegerNullSoZero(totalDiasPorPrecioUnidad));
			reserva.setMontoTotalReserva(Validator.isIntegerNullSoZero(reserva.getSubTotalDias()));
			reservaRepository.save(reserva);
			unidadRepository.save(unidad);
		}else{
			log.error(String.format("No se encuentra la unida con id %s",reservaDTO.getIdUnidad()));
		}


	}

	@Override
	public Reserva findReservaById(Long idReserva) {
		return reservaRepository.findById(idReserva).orElse(null);

	}

	@Override
	public List<Reserva> getAllReservas() {
		return (List<Reserva>) reservaRepository.findAll();
	}

	@Override
	public List<Reserva> getAllReservasByRutPersona(Long rutPersona) {
		return reservaRepository.findAllByRutPersona(rutPersona);
	}

	@Override
	@Transactional
	public void updateStatusReserva(Reserva reservaDTO) {

		reservaRepository.save(findReservaById(reservaDTO.getIdReserva()));
	}

}
