package com.duoc.turimo.app.bl.services;

import com.duoc.turimo.app.bt.IRegionRepository;
import com.duoc.turimo.app.bt.IregionService;
import com.duoc.turimo.app.dl.model.Region;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class RegionServiceImpl implements IregionService {

    @Autowired
    IRegionRepository regionRepository;

    @Override
    public Region getRegionById(Long id) {
        return regionRepository.findById(id).orElse(null);
    }

    @Override
    public List<Region> getAllRegiones() {
        return (List<Region>) regionRepository.findAll();
    }
}
