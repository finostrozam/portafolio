package com.duoc.turimo.app.bl.services;

import com.duoc.turimo.app.bt.IRolRepository;
import com.duoc.turimo.app.bt.IRolService;
import com.duoc.turimo.app.dl.model.Rol;
import com.duoc.turimo.app.dl.model.UsuarioDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RolService implements IRolService {

    @Autowired
    IRolRepository rolRepository;

    @Override
    public void addRol(Rol rol) {
        rolRepository.save(rol);
    }

    @Override
    public Rol findRolById(Long idRol) {
        return rolRepository.findById(idRol).orElse(null);
    }

    @Override
    public List<Rol> getAllRoles() {
        return (List<Rol>) rolRepository.findAll();
    }
}
