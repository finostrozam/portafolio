package com.duoc.turimo.app.bl.services;

import com.duoc.turimo.app.bt.IInventarioRepository;
import com.duoc.turimo.app.bt.IInventarioService;
import com.duoc.turimo.app.dl.model.Inventario;
import com.duoc.turimo.app.dl.model.InventarioAdd;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InventarioServiceImpl implements IInventarioService {

    @Autowired
    IInventarioRepository inventarioRepository;


    @Override
    public List<Inventario> findAll() {
        return inventarioRepository.findAll();
    }

    @Override
    public String detailAll(Long ids) {

        StringBuilder stringBuilder = new StringBuilder();
        inventarioRepository.findAllByIdUnidadFk(ids).forEach( inventario -> {
            stringBuilder.append(inventario.toString()).append("\n");
        } );
        return stringBuilder.toString();
    }

    @Override
    public void addInventarioToUnidad(InventarioAdd inventarioAdd) {

        Inventario inventario = new Inventario();
        inventario.setArticulo(inventarioAdd.getArticulo());
        inventario.setCantidad(inventarioAdd.getCantidad());
        inventario.setIdUnidad(inventarioAdd.getIdUnidad());
        inventarioRepository.save(inventario);

    }

    @Override
    public Inventario findInventarioByIdUnidad(Long idUnidad) {
        return inventarioRepository.findByIdUnidad(idUnidad);
    }

    @Override
    public List<Inventario> findAllInventariosByIdUnidad(Long idUnidad) {
        return inventarioRepository.findAllByIdUnidadFk(idUnidad);
    }
}
