package com.duoc.turimo.app.bl.services;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import com.duoc.turimo.app.bt.IReservaRepository;
import com.duoc.turimo.app.bt.IUnidadRepository;
import com.duoc.turimo.app.dl.model.*;
import com.duoc.turimo.app.utils.DateUtil;
import com.duoc.turimo.app.utils.Validator;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.duoc.turimo.app.bt.IPasajeroRepository;
import com.duoc.turimo.app.bt.IPasajeroService;

@Service
@Slf4j
public class PasajeroServiceImpl implements IPasajeroService {

	@Autowired
	private IPasajeroRepository pasajeroRepository;

	@Autowired
	private IReservaRepository reservaRepository;

	@Autowired
	protected ModelMapper modelMapper;

	@Autowired
	private IUnidadRepository unidadRepository;

	@Override
	@Transactional
	public void addPasajero(PasajeroDTOAdd pasajeroDTO) {
		Reserva reserva = reservaRepository.findById(pasajeroDTO.getIdReserva()).orElse(null);
		if(reserva!=null){
			Validator.checkInDateValidation(new SimpleDateFormat("yyyy-MM-dd").format(reserva.getFechaIngreso()));
			Pasajero pasajero = new Pasajero();
			pasajero.setIdUnidad(pasajeroDTO.getIdUnidad());
			pasajero.setNombrePasajero(pasajeroDTO.getNombrePasajero());
			pasajero.setRutPasajero(pasajeroDTO.getRutPasajero());
			pasajero.setTelefonoPasajero(pasajeroDTO.getTelefonoPasajero());
			pasajero.setIdReserva(pasajeroDTO.getIdReserva());
			pasajeroRepository.save(pasajero);

			List<Pasajero> allPasajeros = pasajeroRepository.findAllByIdReservaAndIdUnidad(pasajero.getIdReserva(),pasajero.getIdUnidad());
			int pasajerosDistintos = io.vavr.collection.List.ofAll(allPasajeros).distinctBy(Pasajero::getRutPasajero).toJavaList().size();
			reserva.setCantPasajeros(pasajerosDistintos);
		}else{
			log.error(String.format("No se encuentra la reserva con id %s",pasajeroDTO.getIdReserva()));
		}




	}

	@Override
	public PasajeroDTO findPasajeroById(Long idPasajero) {
		Pasajero pasajero = pasajeroRepository.findById(idPasajero).orElse(null);
		PasajeroDTO pasajeroDTO = null;
		if(pasajero != null) {

			pasajeroDTO = new PasajeroDTO();
			pasajeroDTO.setIdUnidad(pasajero.getIdUnidad());
			pasajeroDTO.setNombrePasajero(pasajero.getNombrePasajero());
			pasajeroDTO.setRutPasajero(pasajero.getRutPasajero());
			pasajeroDTO.setTelefonoPasajero(pasajero.getTelefonoPasajero());
		}
		return pasajeroDTO;
	}

	@Override
	public List<PasajeroDTO> getAllPasajeros() {
		List<Pasajero> pasajeros = (List<Pasajero>) pasajeroRepository.findAll();
		List<PasajeroDTO> pasajerosDTO = new ArrayList<>();
		pasajeros.forEach(pasajero -> {
			PasajeroDTO pasajeroDTO = new PasajeroDTO();
			pasajeroDTO.setIdUnidad(pasajero.getIdUnidad());
			pasajeroDTO.setNombrePasajero(pasajero.getNombrePasajero());
			pasajeroDTO.setRutPasajero(pasajero.getRutPasajero());
			pasajeroDTO.setTelefonoPasajero(pasajero.getTelefonoPasajero());
			pasajerosDTO.add(pasajeroDTO);

		});
 		return pasajerosDTO;
	}

	@Override
	public List<Pasajero> getAllPasajerosByIdReserva(Long idReserva) {
		List<Pasajero> pasajeros = pasajeroRepository.findAllByIdReserva(idReserva);
		return pasajeros;
	}

	@Override
	@Transactional
	public void updateStatusPasajero(PasajeroDTO pasajeroDTO) {

		if(pasajeroRepository.existsById(pasajeroDTO.getIdPasajero())) {

			Pasajero pasajero = pasajeroRepository.findById(pasajeroDTO.getIdPasajero()).orElse(null);
			if(pasajero!=null){
				pasajero.setIdUnidad(pasajeroDTO.getIdUnidad());
				pasajero.setNombrePasajero(pasajeroDTO.getNombrePasajero());
				pasajero.setRutPasajero(pasajeroDTO.getRutPasajero());
				pasajero.setTelefonoPasajero(pasajeroDTO.getTelefonoPasajero());
				pasajeroRepository.save(pasajero);
			}else{
				log.error(String.format("No se pudo obtener el pasajero con id %s",pasajeroDTO.getIdPasajero()) );
			}


		}

	}

}
