package com.duoc.turimo.app.bl.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.duoc.turimo.app.dl.model.Unidad;
import com.duoc.turimo.app.dl.model.UnidadDTO;
import com.duoc.turimo.app.dl.model.UnidadDTOAdd;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.duoc.turimo.app.bt.IUnidadRepository;
import com.duoc.turimo.app.bt.IUnidadService;

@Service
public class UnidadServiceImpl implements IUnidadService {

	@Autowired
	private IUnidadRepository repositorioUnidad;

	@Autowired
	ModelMapper modelMapper;
	
	@Override
	public void addUnidad(UnidadDTOAdd unidadDTO) {

		repositorioUnidad.save(modelMapper.map(unidadDTO,Unidad.class));

	}

	@Override
	public void actualizaUnidad(UnidadDTO unidad){
		repositorioUnidad.save(modelMapper.map(unidad,Unidad.class));
	}

	@Override
	public UnidadDTO findUnidadById(Long idUnidad) {
		return modelMapper.map(repositorioUnidad.findById(idUnidad).orElse(null),UnidadDTO.class);
	}

	@Override
	public List<UnidadDTO> getAllUnidades() {
		List<UnidadDTO> listaUnidadDTO = new ArrayList<>();
		repositorioUnidad.findAll().forEach(unidad -> { listaUnidadDTO.add(modelMapper.map(unidad,UnidadDTO.class)); });
		return listaUnidadDTO;
	}

}
