package com.duoc.turimo.app.bl.services;

import com.duoc.turimo.app.bt.IDetalleUnidadRepository;
import com.duoc.turimo.app.bt.IDetalleUnidadService;
import com.duoc.turimo.app.dl.model.UnidadDetalle;
import com.duoc.turimo.app.dl.model.UnidadDetalleAdd;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class DetalleUnidadServiceImpl implements IDetalleUnidadService {

    @Autowired
    IDetalleUnidadRepository iDetalleUnidadRepository;

    @Override
    public void saveImagenUnidad(UnidadDetalleAdd unidadDetalleAdd) {
        UnidadDetalle unidadDetalle = new UnidadDetalle();
        unidadDetalle.setIdUnidad(unidadDetalleAdd.getIdUnidad());
        unidadDetalle.setUrlImagen(unidadDetalleAdd.getUrlImagen());
        unidadDetalle.setDescripcion(unidadDetalleAdd.getDescripcion());
        iDetalleUnidadRepository.save(unidadDetalle);
    }

    @Override
    public List<UnidadDetalle> getAllImagesByIdUnidad(Long idUnidad) {
        return iDetalleUnidadRepository.findAllByIdUnidad(idUnidad);
    }

    @Override
    public void saveAllImages(List<UnidadDetalleAdd> detallesUnidades) {
        detallesUnidades.forEach(unidadDetalleAdd -> {
            saveImagenUnidad(unidadDetalleAdd);
        });

    }
}
