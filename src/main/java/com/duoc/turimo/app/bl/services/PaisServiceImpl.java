package com.duoc.turimo.app.bl.services;

import com.duoc.turimo.app.bt.IPaisRepository;
import com.duoc.turimo.app.bt.IPaisService;
import com.duoc.turimo.app.dl.model.Pais;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PaisServiceImpl implements IPaisService {

    @Autowired
    IPaisRepository paisRepository;

    @Override
    public Pais getPais(Long id) {
        return paisRepository.findById(id).orElse(null);
    }

    @Override
    public List<Pais> getAllPaises() {
        return (List<Pais>) paisRepository.findAll();

    }
}
