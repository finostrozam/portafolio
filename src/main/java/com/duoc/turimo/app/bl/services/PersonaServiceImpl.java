package com.duoc.turimo.app.bl.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.duoc.turimo.app.dl.model.Persona;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.duoc.turimo.app.bt.IPersonaRepository;
import com.duoc.turimo.app.bt.IPersonaService;
import com.duoc.turimo.app.dl.model.PersonaDTO;

@Service
public class PersonaServiceImpl implements IPersonaService {

	@Autowired
	private IPersonaRepository personaRepository;
	
	@Override
	public void addPersona(PersonaDTO personaDTO) {
		
		if(personaDTO != null) {
			Persona persona = new Persona();
			persona.setApellidoPersona(personaDTO.getApellidoPersona());
			persona.setCorreoPersona(personaDTO.getCorreoPersona());
			persona.setFechaNacimientoPersona(personaDTO.getFechaNacimientoPersona());
			persona.setNacionalidaPersona(personaDTO.getNacionalidaPersona());
			persona.setNombrePersona(personaDTO.getNombrePersona());
			persona.setRutPersona(personaDTO.getRutPersona());
			personaRepository.save(persona);
		}
	}

	@Override
	public PersonaDTO findPersonaById(Long idPersona) {
		PersonaDTO personaDTO = null;
		Persona persona = personaRepository.findById(idPersona).orElse(null);
		if(persona != null) {

			personaDTO = new PersonaDTO();
			personaDTO.setApellidoPersona(persona.getApellidoPersona());
			personaDTO.setCorreoPersona(persona.getCorreoPersona());
			personaDTO.setFechaNacimientoPersona(persona.getFechaNacimientoPersona());
			personaDTO.setNacionalidaPersona(persona.getNacionalidaPersona());
			personaDTO.setNombrePersona(persona.getNombrePersona());
			personaDTO.setRutPersona(persona.getRutPersona());
		}
		return personaDTO;
	}

	@Override
	public List<PersonaDTO> getAllPersonas() {
		List<Persona> persona = (List<Persona>) personaRepository.findAll();
		List<PersonaDTO> personasDTO = new ArrayList<>();
		
		persona.stream().forEach(p ->  {
			PersonaDTO personaDTO = new PersonaDTO();
			personaDTO.setApellidoPersona(p.getApellidoPersona());
			personaDTO.setCorreoPersona(p.getCorreoPersona());
			personaDTO.setFechaNacimientoPersona(p.getFechaNacimientoPersona());
			personaDTO.setNacionalidaPersona(p.getNacionalidaPersona());
			personaDTO.setNombrePersona(p.getNombrePersona());
			personaDTO.setRutPersona(p.getRutPersona());
			personasDTO.add(personaDTO);
		} );
 		return personasDTO;
	}

	@Override
	@Transactional
	public void updateStatusPersona(PersonaDTO personaDTO) {
		
		Optional<Persona> persona = personaRepository.findById(personaDTO.getRutPersona());
		if(persona.isPresent()) { 
			personaRepository.save(persona.get());
		}
	}

	@Override
	public boolean exist(PersonaDTO persona) {
		return personaRepository.existsById(persona.getRutPersona());
	}

	@Transactional
	public Persona devolverPersonaPorID(Long id) {
		Persona persona = null;
		persona = personaRepository.findById(id).orElse(null);
		return persona;
	}

}
