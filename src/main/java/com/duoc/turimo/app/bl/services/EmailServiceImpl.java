package com.duoc.turimo.app.bl.services;

import com.duoc.turimo.app.bt.*;
import com.duoc.turimo.app.dl.model.Persona;
import com.duoc.turimo.app.dl.model.Reserva;
import com.duoc.turimo.app.utils.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class EmailServiceImpl implements EmailService {

    @Autowired
    IPersonaRepository personaRepository;

    @Autowired
    IReservaRepository reservaRepository;

    @Qualifier("getJavaMailSender")
    @Autowired
    private JavaMailSender emailSender;


    public void sendSimpleMessage(String to, String subject, String text) {

        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("turismo.real.duoc.2020@gmail.com");
        message.setTo(to);
        message.setSubject(subject);
        message.setText(text);
        emailSender.send(message);

    }

    @Scheduled(fixedDelay = 21600000)
    @Async
    public void enviarCorreos(){
      log.info("Se recolectan las reservas con un dia antes del CheckIn");
        List<Reserva> reservas = (List<Reserva>) reservaRepository.findAll();

        List<Reserva> reservasFiltradas = reservas.stream()
                .filter(reserva -> DateUtil.esHoyUnDiaAntesDe(new SimpleDateFormat("yyyy-MM-dd")
                .format(reserva.getFechaIngreso())) && !reserva.getAvisoCheckInEnviado())
                .collect(Collectors.toList());

        reservasFiltradas.forEach(reserva -> {
            Persona persona = personaRepository.findById(reserva.getRutPersona()).orElse(null);
            if(persona!=null){
                sendSimpleMessage(persona.getCorreoPersona(),"Correo Prueba","Hola Quetal");
                reserva.setAvisoCheckInEnviado(true);
                reservaRepository.save(reserva);}

        });
        log.info("Se envian los correos por cada elemento");
    }


}