package com.duoc.turimo.app.bl.services;

import com.duoc.turimo.app.bt.IPagosService;
import com.duoc.turimo.app.bt.IReservaRepository;
import com.duoc.turimo.app.dl.model.Reserva;
import com.duoc.turimo.app.utils.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PagoServiceImpl implements IPagosService {

    @Autowired
    IReservaRepository reservaRepository;

    @Override
    public void pagarReserva(Long idReserva,Integer monto) {

        Reserva reserva = reservaRepository.findById(idReserva).orElse(null);
        Validator.nullValidation(reserva);
        reserva.setAbono(Validator.isIntegerNullSoZero(reserva.getAbono())+ Validator.isIntegerNullSoZero(monto));
        reservaRepository.save(reserva);
    }
}
