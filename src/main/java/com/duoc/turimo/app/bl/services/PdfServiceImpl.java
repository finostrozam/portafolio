package com.duoc.turimo.app.bl.services;

import com.duoc.turimo.app.bt.*;
import com.duoc.turimo.app.dl.model.Inventario;
import com.duoc.turimo.app.dl.model.Persona;
import com.duoc.turimo.app.dl.model.Reserva;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class PdfServiceImpl implements IPdfService {

    @Autowired
    IPersonaRepository personaRepository;

    @Autowired
    IInventarioService inventarioService;

    @Autowired
    IReservaRepository reservaRepository;

    @Override
    public byte[] createCheckInPDFFromIdReserva(Long idReserva) throws Exception {
        Document document = new Document();
        ByteArrayOutputStream ba = new ByteArrayOutputStream();
        try {
            PdfWriter.getInstance(document, ba);
            document.open();
            document.addTitle("Reporte turismo Real");
            document.addSubject(String.format("Reporte de: %s", "colocar a que corresponde"));
            document.addKeywords("turismo,ofertas,duoc");
            document.addAuthor("Felipe Inostroza");
            document.addCreator("Felipe Inostroza");

            Paragraph preface = new Paragraph();
            // We add one empty line
            addEmptyLine(preface, 1);
            // Lets write a big header
            Font titulo = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 24, BaseColor.BLACK);
            preface.add(new Paragraph("Acta de Check In", titulo));
            preface.setAlignment(Element.ALIGN_CENTER);

            addEmptyLine(preface, 1);

            Reserva reserva = reservaRepository.findById(idReserva).orElse(null);
            Font font = FontFactory.getFont(FontFactory.COURIER, 12, BaseColor.BLACK);

            String Parrafo = String.format("Primero :\n %s \n Segundo:\n %s  ", getParrafoCheck(reserva),getInventarioToString(reserva)) ;
            Chunk chunk = new Chunk(Parrafo, font);


            // document.add(titulo_ch);
            document.add(preface);
            document.add(chunk);
            document.close();

        } catch (DocumentException exception) {
            log.error(String.format("No se puede obtener la instancia del documento, causa %s", exception.getCause()));
        }
        return ba.toByteArray();
    }

    @Override
    public byte[] createCheckOutPDFFromIdReserva(Long idReserva,String nombreArticulo,int valor) throws Exception {
        Document document = new Document();
        ByteArrayOutputStream ba = new ByteArrayOutputStream();
        try {
            PdfWriter.getInstance(document, ba);
            document.open();
            document.addTitle("Reporte turismo Real");
            document.addSubject(String.format("Reporte de: %s", "colocar a que corresponde"));
            document.addKeywords("turismo,ofertas,duoc");
            document.addAuthor("Felipe Inostroza");
            document.addCreator("Felipe Inostroza");

            Paragraph preface = new Paragraph();
            // We add one empty line
            addEmptyLine(preface, 1);
            // Lets write a big header
            Font titulo = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 24, BaseColor.BLACK);
            preface.add(new Paragraph("Acta de Check Out por Siniestro:", titulo));
            preface.setAlignment(Element.ALIGN_CENTER);

            addEmptyLine(preface, 1);

            Reserva reserva = reservaRepository.findById(idReserva).orElse(null);
            Font font = FontFactory.getFont(FontFactory.COURIER, 12, BaseColor.BLACK);

            String Parrafo = String.format("Primero :\n %s ", getParrafoCheckOut(reserva,nombreArticulo,valor)) ;
            Chunk chunk = new Chunk(Parrafo, font);


            // document.add(titulo_ch);
            document.add(preface);
            document.add(chunk);
            document.close();
            reserva.setMontoTotalReserva(reserva.getMontoTotalReserva()+valor);
            reservaRepository.save(reserva);

        } catch (DocumentException exception) {
            log.error(String.format("No se puede obtener la instancia del documento, causa %s", exception.getCause()));
        }
        return ba.toByteArray();
    }


    private static void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }

    }

    private String getParrafoCheck(Reserva reserva){
        Persona cliente = personaRepository.findById(reserva.getRutPersona()).orElse(null);

        String parrafo = String.format(
                "El siguiente documento es una declaración donde el cliente con Nombre %s %s  y Rut %s acepta expresamente" +
                "que todos los articulos que se encuentran detallados en la presente acta, están debidamente en el inmueble a " +
                "reservar y utilizar, por lo que tambien se expresa que será de responsabilidad del arrendatario el correcto uso de cada uno" +
                " de los articulos descritos en la presente.\n En el caso que cualquiera de los articulos u artefactos sufrieran daños," +
                "accidentales o intencionales, estos deben ser reemplazados por el propietario de forma monetaria o en su defecto" +
                "por el mismo árticulo de igual o mayor valor, siempre que este permita el reemplazo del artefacto siniestrado manteniendo la gamma" +
                "y calidad que el cliente recibe a su vez.\n La presente acta de CheckIn a su vez indica que el usuario hará ingreso al inmueble o unidad" +
                "el dia %s y hará abandono de este mismo el dia %s antes de la 12:00 PM. Por lo que si el propietario no dejase el inmueble en la fecha" +
                "que actualmente detalla el presente documento, será notificado que se incurrirá en un cobro adicional por un día completo.\n" +
                "",cliente.getNombrePersona(),cliente.getApellidoPersona(),cliente.getRutPersona(),reserva.getFechaIngreso(),reserva.getFechaSalida() ) ;
        return parrafo;
    }

    private String getParrafoCheckOut(Reserva reserva, String nombreArticulo, int valor){
        Persona cliente = personaRepository.findById(reserva.getRutPersona()).orElse(null);

        String parrafo = String.format(
                "El siguiente documento se imprime por la necesidad de incurrir en el cobro de %s por conceptos de perdida o daño del articulo detallado" +
                        "su monto es de %s y será sumado al total de su reserva que asiende a : %s",
                nombreArticulo,
                valor,
                reserva.getMontoTotalReserva() + valor) ;
        return parrafo;
    }
/*
     @Override
    public byte[] createReportPDF() throws Exception {
        Document document = new Document();
        ByteArrayOutputStream ba = new ByteArrayOutputStream();
        try {
            PdfWriter.getInstance(document, ba);
            document.open();
            document.addTitle("Reporte turismo Real");
            document.addSubject(String.format("Reporte de: %s", "colocar a que corresponde"));
            document.addKeywords("turismo,ofertas,duoc");
            document.addAuthor("Felipe Inostroza");
            document.addCreator("Felipe Inostroza");

            Paragraph preface = new Paragraph();
            // We add one empty line
            addEmptyLine(preface, 1);
            // Lets write a big header
            Font titulo = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 24, BaseColor.BLACK);
            preface.add(new Paragraph("Acta de Check In", titulo));
            preface.setAlignment(Element.ALIGN_CENTER);

            addEmptyLine(preface, 1);


            Font font = FontFactory.getFont(FontFactory.COURIER, 12, BaseColor.BLACK);

            String Parrafo = String.format("Primero :\n %s \n Segundo:\n %s  ", getParrafoCheck(reserva),getInventarioToString(reserva)) ;
            Chunk chunk = new Chunk(Parrafo, font);


            // document.add(titulo_ch);
            document.add(preface);
            document.add(chunk);
            document.close();

        } catch (DocumentException exception) {
            log.error(String.format("No se puede obtener la instancia del documento, causa %s", exception.getCause()));
        }
        return ba.toByteArray();
    }



    private String getParrafoCheckOut(){
        String parrafo = String.format(
                "El siguiente documento se imprime y contiene las reservas por semanal: \n %s \n mensual: \n %s \n anual: \n %s \n",
                nombreArticulo,
                valor,
                reserva.getMontoTotalReserva() + valor) ;
        return parrafo;
    }

    private int sumaMontosTotalesFromReservas(List<Reserva> reservas){
        return reservas.stream().mapToInt(reserva ->reserva.getMontoTotalReserva()).sum();
    }


    private List<Reserva> getReservasTotalesByMonth(int month, int year){

    }

    private List<Reserva> getReservasTotalesByWeek(java.util.Date fechaInicial, int semanas){

    }

    private List<Reserva> getReservasTotalesByYear(int year){

    }
*/
    private String getInventarioToString(Reserva reserva){
        return inventarioService.detailAll(reserva.getIdUnidad());
    }
}
