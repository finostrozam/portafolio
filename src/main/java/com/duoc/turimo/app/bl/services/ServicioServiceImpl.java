package com.duoc.turimo.app.bl.services;

import java.util.ArrayList;
import java.util.List;

import com.duoc.turimo.app.dl.model.Servicio;
import com.duoc.turimo.app.dl.model.ServicioDTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.duoc.turimo.app.bt.IServicioRepository;
import com.duoc.turimo.app.bt.IServicioService;

@Service
public class ServicioServiceImpl implements IServicioService {

	@Autowired
	private IServicioRepository servicioRepository;

	@Autowired
	private ModelMapper modelMapper;
	
	@Override
	@Transactional
	public void addServicio(ServicioDTO servicioDTO) {


		if(servicioDTO != null) {
			Servicio servicio = new Servicio();
			servicio.setMontoIva(servicioDTO.getMontoIva());
			servicio.setMontoServicio(servicioDTO.getMontoServicio());
			servicio.setNombreServicio(servicioDTO.getNombreServicio());
			servicio.setIdProveedor(servicioDTO.getIdProveedor());


			servicioRepository.save(servicio);

		}

	}

	@Override
	public ServicioDTO findServicioById(Long idServicio) {
		Servicio servicio = servicioRepository.findById(idServicio).orElse(null);
		ServicioDTO servicioDTO = null;
		if(servicio != null) {

			servicioDTO = new ServicioDTO();
			servicioDTO.setIdProveedor(servicio.getIdProveedor());
			servicioDTO.setIdServicio(servicio.getIdServicio());
			servicioDTO.setMontoIva(servicio.getMontoIva());
			servicioDTO.setMontoServicio(servicio.getMontoServicio());
			servicioDTO.setNombreServicio(servicio.getNombreServicio());
		}
		return servicioDTO;
	}



	@Override
	public List<ServicioDTO> getAllServicios() {
		List<Servicio> servicios = (List<Servicio>) servicioRepository.findAll();
		List<ServicioDTO> serviciosDTO = new ArrayList<>();
 		servicios.stream().forEach(servicio -> {
			ServicioDTO servicioDTO = new ServicioDTO();
			servicioDTO.setIdProveedor(servicio.getIdProveedor());
			servicioDTO.setIdServicio(servicio.getIdServicio());
			servicioDTO.setMontoIva(servicio.getMontoIva());
			servicioDTO.setMontoServicio(servicio.getMontoServicio());
			servicioDTO.setNombreServicio(servicio.getNombreServicio());
			servicioDTO.setTipo(servicio.getTipo());
			servicioDTO.setDescripcion(servicio.getDescripcion());
			serviciosDTO.add(servicioDTO);
		});
		return serviciosDTO;
	}

	@Override
	@Transactional
	public void updateStatusServicio(ServicioDTO servicioDTO) {

		if(servicioDTO != null) {
			Servicio servicio = servicioRepository.findById(servicioDTO.getIdServicio()).orElse(null);
			servicio.setMontoIva(servicioDTO.getMontoIva());
			servicio.setMontoServicio(servicioDTO.getMontoServicio());
			servicio.setNombreServicio(servicioDTO.getNombreServicio());
			servicio.setIdProveedor(servicioDTO.getIdProveedor());
			servicioRepository.save(servicio);

		}

	}

}
