package com.duoc.turimo.app.bl.services;

import java.util.List;

import com.duoc.turimo.app.dl.model.Transaccion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.duoc.turimo.app.bt.ITransaccionRepository;
import com.duoc.turimo.app.bt.ITransaccionService;

@Service
public class TransaccionServiceImpl implements ITransaccionService {

	@Autowired
	private ITransaccionRepository transaccionRepository;
	
	@Override
	@Transactional
	public boolean addTransaccion(Transaccion transaccion) {
		boolean respuesta = false;
		if(transaccion != null && !transaccionRepository.existsById(transaccion.getIdtransaccion())) {
			transaccionRepository.save(transaccion);
		    respuesta = true;
		}
		return respuesta;
	}

	@Override
	public Transaccion findTransaccionById(Long idTransaccion) {
		Transaccion transaccion = null;
		if(idTransaccion != null) {
			transaccion = transaccionRepository.findById(idTransaccion).orElse(null);	
		}
		return transaccion;
	}

	@Override
	public List<Transaccion> getAllTransaccions() {
		List<Transaccion> transacciones = (List<Transaccion>) transaccionRepository.findAll();
 		return transacciones;
	}

	@Override
	@Transactional
	public boolean updateStatusTransaccion(Transaccion transaccion) {
		boolean respuesta = false;
		if(transaccionRepository.existsById(transaccion.getIdtransaccion())) {
			transaccionRepository.save(transaccion);
			respuesta = true; 
		}
		return respuesta;
	}

}
