package com.duoc.turimo.app.bl.services;

import com.duoc.turimo.app.bt.IProveedorRepository;
import com.duoc.turimo.app.bt.IProveedorService;
import com.duoc.turimo.app.dl.model.Proveedor;
import com.duoc.turimo.app.dl.model.ProveedorAdd;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ProveedorServiceImpl implements IProveedorService {

    @Autowired
    IProveedorRepository iProveedorRepository;

    @Override
    public Proveedor getProveedorById(Long id) {
        return iProveedorRepository.findById(id).orElse(null);
    }

    @Override
    public List<Proveedor> getAllProveedores() {
        return  (List<Proveedor>) iProveedorRepository.findAll();
    }

    @Override
    public void addProveedor(ProveedorAdd proveedor) {
        iProveedorRepository.save(proveedor);
    }

    @Override
    public void aupdateProveedor(Proveedor proveedor) {
        iProveedorRepository.save(proveedor);
    }
}
