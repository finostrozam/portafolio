package com.duoc.turimo.app.utils;

import com.duoc.turimo.app.dl.model.ServicioDetalle;

import java.util.List;

public class CalculatorUtil {

    public static int getTotalByQtyPerPrice(int days,int price){
        return price * days;
    }

    public static int getSumTotalForEachElementOnServicioDetalle(List<ServicioDetalle> servicios){
        return servicios.stream().mapToInt(elemento -> elemento.getTotal()).sum();
    }

    public static int getSumQtyForEachElementOnServicioDetalle(List<ServicioDetalle> servicios){
        return servicios.stream().mapToInt(elemento -> elemento.getQty()).sum();
    }
}
