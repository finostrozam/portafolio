package com.duoc.turimo.app.utils;


import lombok.extern.slf4j.Slf4j;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.concurrent.TimeUnit;
@Slf4j
public class DateUtil {
	
	public static java.sql.Date getSQLDate(String dateFormat,String date){
		java.sql.Date fechaResponse = null;
		SimpleDateFormat dateFormater = new SimpleDateFormat(dateFormat);
		try {
			java.util.Date fechaToTransform = dateFormater.parse(date);
			fechaResponse = new Date(fechaToTransform.getTime());
		} catch (ParseException e) {
			e.getMessage();
		}
		return fechaResponse;
	}

	public static java.sql.Date getSQLDate(String dateFormat){

		SimpleDateFormat dateFormater = new SimpleDateFormat(dateFormat);
		java.util.Date fechaToTransform = dateFormater.getCalendar().getTime();
		return new Date(fechaToTransform.getTime());
	}

	public static int qtyOfBetweenDays(String initialDate , String finalDate){
		long diffInMillies = 0;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		try{
			diffInMillies = Math.abs(sdf.parse(finalDate).getTime() - sdf.parse(initialDate).getTime());
		}catch(Exception e) {
			log.error(String.format("No se ha podido parsear las fechas fecha inicial : %s y Fecha Final : %s", initialDate, finalDate));
		}
		return (int) TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
	}

	public static boolean isDisponible(String checkoutDay){
		long diffInMillies = 0;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date hoy = new java.util.Date();
		try{
			diffInMillies =  sdf.parse(checkoutDay).compareTo(sdf.parse(sdf.format(hoy)));
		}catch(Exception e){
			log.error(String.format("No se ha podido parsear la fecha de checkout : %s ",checkoutDay));
		}
		return diffInMillies < 0;
	}

	public static int isBefore(String day){
		int diffInMillies = 0;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date hoy = new java.util.Date();
		try{
			diffInMillies =  sdf.parse(day).compareTo(sdf.parse(sdf.format(hoy)));
		}catch(Exception e){
			log.error(String.format("No se ha podido parsear la fecha de checkout : %s ",day));
		}
		return diffInMillies;
	}

	public static boolean isCorrectOrderBetweenTwoDates(String initialDate,String finalDate){
		boolean respuesta = false;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try{
			respuesta =  sdf.parse(initialDate).before(sdf.parse(finalDate));
		}catch(Exception e){
			log.error(String.format("No se ha podido parsear las fechas inicial [%s] final [%s]",initialDate,finalDate));
		}
		return respuesta;
	}



	public static boolean esHoyUnDiaAntesDe(String fechaAConsultar) {
		boolean respuesta = false;
		java.util.Date hoy = new java.util.Date();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyy-MM-dd");
		if(isCorrectOrderBetweenTwoDates( simpleDateFormat.format(hoy),fechaAConsultar)){
			try {
				if(hoy.before(simpleDateFormat.parse(fechaAConsultar)) && qtyOfBetweenDays(simpleDateFormat.format(hoy) , fechaAConsultar) ==1){
					respuesta = true;
				}
			} catch (ParseException e) {
				log.error("No se puede parsear la fecha");
			}

		}
		return  respuesta;
	}

	public static java.util.Date getJavaUtilDate(String fecha , String formato) {
		java.util.Date fechaRetorno = null;
		try{
			fechaRetorno = new SimpleDateFormat(formato).parse(fecha);
		}catch (ParseException exception){
			log.error(String.format("No se ha podido parsear la fecha %s",fecha));
		}
		return  fechaRetorno;
	}

}
