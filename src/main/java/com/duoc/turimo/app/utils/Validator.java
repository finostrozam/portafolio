package com.duoc.turimo.app.utils;

import com.duoc.turimo.app.emuns.Errores;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import org.springframework.web.server.ResponseStatusException;

import static com.duoc.turimo.app.utils.DateUtil.isBefore;
import static com.duoc.turimo.app.utils.DateUtil.isCorrectOrderBetweenTwoDates;


public class Validator {

    static final int HOY = 0;

    public static void nullValidation(Object object) throws ResponseStatusException{
        if(object == null){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "La entidad buscada en persistencia retorna null");
        }
    }

    public static void obligatorioValidation(String obligatorio) throws ResponseStatusException{
        if(StringUtils.isEmpty(obligatorio)){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Uno de los campos que envia como vacio es obligatorio!");
        }
    }

    public static void checkInDateValidation(String checkinDate) throws ResponseStatusException{

        if(isBefore(checkinDate)<HOY){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, Errores.FECHAS_ERRONEAS_CHECKIN.getStringValue());
        }
    }
    public static void checkOutDateValidation(String checkOutDate) throws ResponseStatusException{

        if(isBefore(checkOutDate)<HOY){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, Errores.FECHAS_ERRONEAS_CHECKOUT.getStringValue());
        }
    }

    public static void checkDatesOrder(String checkIn,String checkOut) throws ResponseStatusException{

        if(!isCorrectOrderBetweenTwoDates( checkIn,checkOut)){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, Errores.FECHAS_ERRONEAS_ORDEN.getStringValue());
        }
    }

    public static void isDisponible(boolean isDisponible) throws ResponseStatusException{

        if(!isDisponible){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, Errores.UNIDAD_NO_DISPONIBLE.getStringValue());
        }
    }

    public static int isIntegerNullSoZero(Integer value) throws ResponseStatusException{
        return value == null ? 0 : value;
    }

    public static void checkIsAntesDeCheckin(boolean isUndiaAntes) throws ResponseStatusException{

        if(!isUndiaAntes){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, Errores.FECHAS_ERRONEAS_ANTES_CHECKIN.getStringValue());
        }
    }


}
